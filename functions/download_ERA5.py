


import cdsapi
c = cdsapi.Client()

output = "/storage/karger/era5_pr/"
for year in range(1981,2010):
    c.retrieve(
        'derived-era5-single-levels-daily-statistics',
        {
            "product_type": "reanalysis",
            "variable": ["total_precipitation"],
            "year": str(year),
            "month": [
                "01", "02", "03",
                "04", "05", "06",
                "07", "08", "09",
                "10", "11", "12"
            ],
            "day": [
                "01", "02", "03",
                "04", "05", "06",
                "07", "08", "09",
                "10", "11", "12",
                "13", "14", "15",
                "16", "17", "18",
                "19", "20", "21",
                "22", "23", "24",
                "25", "26", "27",
                "28", "29", "30",
                "31"
            ],
            "daily_statistic": "daily_mean",
            "time_zone": "utc+00:00",
            "frequency": "1_hourly",
            "format": "netcdf"
        },
        output + "era5_pr_" + str(year) + ".nc")