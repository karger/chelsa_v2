from functions.saga_functions import grid_calculator_simple
import os
import saga_api

def convert2cog(obj, tmp, filename, scale, offset, ot, st,
                cf_standard_name, variable, variable_long_name, variable_unit, freq='day', compress='DEFLATE',
                overwrite_offset=True, force_crs=True):
    """
    Scale and save CSG_Data_Object as a scaled and compressed cloud-optimized GeoTIFF (COG),
    ensure the correct coordinate reference system (CRS), and reproject it to WGS84 (EPSG:4326).

    :param obj: grid object, CSG_Data_Object
    :param tmp: path to the temporary directory, string
    :param filename: output filename, string
    :param scale: scale, float
    :param offset: offset, float
    :param ot: output type, string
    :param st: output type tmp grid, integer
    :param cf_standard_name: CF standard variable name, string
    :param variable: variable short name, string
    :param variable_long_name: variable long name, string
    :param variable_unit: unit, string
    :param freq: frequency, string, default='day'
    :param compress: compression type, string [DEFLATE, LZW, ZSTD, LERC_ZSTD]
    :param force_crs: Boolean, if True, forces the correct CRS to EPSG:32628
    :return: True
    :rtype: bool
    """
    scale_calc = 1 / scale
    offset_calc = offset * (-1)
    date = "01-06-1996"

    if overwrite_offset:
        offset_calc = 0.0

    obj2 = grid_calculator_simple(obj1=obj,
                                  equ=f'(a+{offset})*{scale_calc}',
                                  type=st)

    tmp_sdat_file = os.path.join(tmp, 'tmp_outfile.sdat')
    obj2.Save(tmp_sdat_file)

    # Step 0: Convert SAGA GRID (.sdat) to GeoTIFF using gdalwarp (not gdal_translate)
    tmp_geotiff_file = os.path.join(tmp, 'tmp_geotiff.tif')

    sysc_convert = (f'gdalwarp -of GTiff {tmp_sdat_file} {tmp_geotiff_file}')
    print(sysc_convert)
    os.system(sysc_convert)

    # Step 1: Ensure the correct CRS is set (EPSG:32628)
    if force_crs:
        sysc_fix_crs = f'gdal_edit.py -a_srs EPSG:32628 {tmp_geotiff_file}'
        print(sysc_fix_crs)
        os.system(sysc_fix_crs)

    # Define compression settings
    compression_options = {
        'DEFLATE': ' -co "COMPRESS=DEFLATE" -co "PREDICTOR=2" -co "LEVEL=9" ',
        'LZW': ' -co "COMPRESS=LZW" ',
        'ZSTD': ' -co "COMPRESS=ZSTD" -co "PREDICTOR=2" ',
        'LERC': ' -co "COMPRESS=LERC_ZSTD" -co "ZSTD_LEVEL=15" -co "MAX_Z_ERROR=5" '
    }
    cstr = compression_options.get(compress, ' -co "COMPRESS=DEFLATE" ')

    # Step 2: Convert to COG
    tmp_cog_file = os.path.join(tmp, 'tmp_cog.tif')
    sysc_translate = (f'gdal_translate -of COG -a_scale {scale} -a_offset {offset_calc} -ot {ot} ' +
                      cstr +
                      ' -co "NUM_THREADS=ALL_CPUS" ' +
                      f' -mo forcing="CHELSA_V1.2" ' +
                      f' -mo activity_id="observation" ' +
                      f' -mo cf_standard_name="{cf_standard_name}" ' +
                      f' -mo experiment_id="observation" ' +
                      f' -mo institution_id="WSL" ' +
                      f' -mo product="model-output" ' +
                      f' -mo project="CHELSA" ' +
                      f' -mo source_id="CanaryClimV1.0" ' +
                      f' -mo variable="{variable}" ' +
                      f' -mo variable_long_name="{variable_long_name}" ' +
                      f' -mo variable_unit="{variable_unit}" ' +
                      f' -mo frequency="{freq}" ' +
                      f' -mo version="1.0" ' +
                      f' -mo datetime="{date}" ' +
                      f' -mo citation1="Patiño, J., Collart, F., Vanderpoorten, A.; Martin-Esquivel, J.L., Naranjo-Cigala, A., Mirolo, S., Karger, D.N. (2023). Spatial resolution impacts projected plant responses to climate change on topographically complex islands. Diversity and Distributions 29(10) 1245-1262." ' +
                      f' -mo data_cite="Collart, F., Patiño, J., Vanderpoorten, A.; Martin-Esquivel, J.L., Naranjo-Cigala, A., Mirolo, S., Karger, D.N. (2023). CanaryClim - Climatic maps. Figshare. https://doi.org/10.6084/m9.figshare.22060340.v2" ' +
                      f' -mo contact="dirk.karger@wsl.ch" ' +
                      tmp_geotiff_file + ' ' + tmp_cog_file)

    print(sysc_translate)
    os.system(sysc_translate)

    # Step 3: Reproject to WGS84
    sysc_warp = (f'gdalwarp -t_srs EPSG:4326 -r bilinear ' +
                 f' -co "COMPRESS=DEFLATE" ' +
                 f' -co "TILED=YES" ' +
                 f' -co "NUM_THREADS=ALL_CPUS" ' +
                 tmp_cog_file + ' ' + filename)

    print(sysc_warp)
    os.system(sysc_warp)

    # Clean up temporary files
    os.remove(tmp_cog_file)
    os.remove(tmp_geotiff_file)
    os.remove(tmp_sdat_file)

    # Delete the temporary SAGA object
    saga_api.SG_Get_Data_Manager().Delete(obj2)

    return True
