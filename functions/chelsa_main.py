#!/usr/bin/env python

# This file is part of chelsa_v2.
#
# chelsa_v2 is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# chelsa_v2 is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with chelsa_v2.  If not, see <https://www.gnu.org/licenses/>.
import os.path
import shutil
import psutil
import datetime
from tqdm import tqdm
from functions.logtrack import logtrack
from functions.convert2cog import convert2cog
from functions.saga_functions import *
from functions.bcolors import bcolors

def chelsa(TEMP, OUTPUT, INPUT, SRAD, dayofyear,
           calcprec, calcps, calcsfcWind, calchurs, calctas, calctasmax, calctasmin, calcrsds, calctz,
           keeptemp, month, year, day, overwrite):
    """
    run CHELSA for a specific time

    :param TEMP: path to the temporary directory, string
    :param OUTPUT: path to the output directory, string
    :param INPUT: path to the input directory, string
    :param SRAD: path to the clear sky solar radiation directory, string
    :param dayofyear: day of the year, integer
    :param calcprec: calculation of precipitation, integer, 0=false, 1=true
    :param calcps: calculation of surface pressure, integer, 0=false, 1=true
    :param calcsfcWind: calculation of surface wind speed, integer, 0=false, 1=true
    :param calchurs: calculation of relative humidity, integer, 0=false, 1=true
    :param calctas: calculation of tas, integer, 0=false, 1=true
    :param calctasmax: calculation of tasmax, integer, 0=false, 1=true
    :param calctasmin: calculation of tasmin, integer, 0=false, 1=true
    :param calcrsds: calculation of rsds, integer, 0=false, 1=true
    :param calctz: calculation of temperature lapse rates, integer, 0=false, 1=true
    :param keeptemp: delete the temporary directory at the end, integer, 0=false, 1=true
    :param day: day, integer
    :param month: month, integer
    :param year: year, integer
    :param overwrite: shall existing files be overwritten, integer, 0=false, 1=true

    :return: True
    :rtype: bool
    """

    # set times of the day and levels
    times = ['00:00', '01:00', '02:00',
             '03:00', '04:00', '05:00',
             '06:00', '07:00', '08:00',
             '09:00', '10:00', '11:00',
             '12:00', '13:00', '14:00',
             '15:00', '16:00', '17:00',
             '18:00', '19:00', '20:00',
             '21:00', '22:00', '23:00']


    process = psutil.Process(os.getpid())
    logfile = TEMP + 'log.txt'
    saga_api.SG_Get_Data_Manager().Delete_All()
    print(bcolors.HEADER + bcolors.BOLD + "CHELSA Version 2.1" + bcolors.ENDC)
    print(bcolors.HEADER + "CALCULATION START TIME: " + str(datetime.datetime.now()) + bcolors.ENDC)
    Load_Tool_Libraries(True)

    ### calculate windeffect *******************************************************************************************
    if calchurs == 1 or calcprec == 1 or calcrsds == 1:
        outputfile2 = OUTPUT + 'we/CHELSA_we_' + day + '_' + month + '_' + year + '_V.2.1.tif'
        if os.path.isfile(outputfile2) == False or \
                os.path.isfile((TEMP + 'cblev_ras.sgrd')) == False or overwrite == 1:
            print(bcolors.WARNING + 'overwrite = True or '
                  + outputfile2 + ' does not exists! start calculation' + bcolors.ENDC)
            # Display warning message
            print(
                f"{bcolors.WARNING}overwrite = True or {outputfile2} does not exist! Starting calculation{bcolors.ENDC}")

            # Import required datasets
            uwind = import_ncdf(TEMP + 'u.nc').Get_Grid(0)  # Zonal wind component
            vwind = import_ncdf(TEMP + 'v.nc').Get_Grid(0)  # Meridional wind component

            # Check if 'cbase.nc' exists, otherwise compute it
            cbase_path = TEMP + 'cbase.nc'
            if os.path.isfile(cbase_path):
                cblev = import_ncdf(cbase_path).Get_Grid(0)
            else:
                tas = import_ncdf(TEMP + 'tas_.nc').Get_Grid(0)
                hurs = import_ncdf(TEMP + 'hurs.nc').Get_Grid(0)
                cblev = grid_calculator(tas, hurs, '(20 + ((a - 273.15) / 5)) * (100 - b)')

            # Load digital elevation models
            demproj = load_sagadata(INPUT + 'dem_merc3.sgrd')  # Mercator projection DEM
            dem1 = load_sagadata(INPUT + 'dem_latlong3.sgrd')  # Lat-long projection DEM

            # Debugging: Print variable types to check successful loading
            print(
                f"Loaded variables: uwind={type(uwind)}, vwind={type(vwind)}, cblev={type(cblev)}, demproj={type(demproj)}, dem1={type(dem1)}")

            # Check if any dataset failed to load
            if any(var is None for var in [uwind, vwind, cblev, demproj, dem1]):
                raise ValueError("Error: One or more input datasets failed to load. Check file paths!")

            # Track memory usage
            logtrack(process, 'windeffect: files loaded', logfile)

            # Convert wind data to lat-long coordinate system
            uwind = change_latlong(uwind)
            vwind = change_latlong(vwind)
            cblev = change_latlong(cblev)
            cblev = calc_geopotential(cblev)

            # Debugging: Confirm transformations
            print(f"Transformed variables: uwind={type(uwind)}, vwind={type(vwind)}, cblev={type(cblev)}")

            # Track memory usage
            logtrack(process, 'windeffect: change longitudinal range', logfile)

            # Set coordinate reference system
            set_2_latlong(uwind)
            set_2_latlong(vwind)
            set_2_latlong(cblev)

            # Convert grid values to shapefile format for reprojection
            uwind_shp = gridvalues_to_points(uwind)
            vwind_shp = gridvalues_to_points(vwind)
            cblev_shp = gridvalues_to_points(cblev)

            # Track memory usage
            logtrack(process, 'windeffect: change projection', logfile)

            # Reproject wind data to Mercator projection
            uwind_shp = reproject_shape(uwind_shp)
            vwind_shp = reproject_shape(vwind_shp)

            # Apply multilevel B-spline interpolation
            uwind_ras = multilevel_B_spline(uwind_shp, demproj)
            vwind_ras = multilevel_B_spline(vwind_shp, demproj)

            # Calculate wind direction from interpolated wind components
            direction = polar_coords(uwind_ras, vwind_ras)

            # Track memory usage
            logtrack(process, 'windeffect: b spline', logfile)

            # Clean up memory for intermediate raster files
            saga_api.SG_Get_Data_Manager().Delete(uwind_ras)
            saga_api.SG_Get_Data_Manager().Delete(vwind_ras)

            # Track memory usage
            logtrack(process, 'windeffect: start loading dem', logfile)

            # Process DEM data
            dem = change_data_storage(dem1)
            cblev_ras = multilevel_B_spline(cblev_shp, dem)
            cblev_ras.Save(TEMP + 'cblev_ras.sgrd')

            # Compute wind effect
            windef = windeffect(direction, demproj)
            windef1 = proj_2_latlong(windef, dem)

            # Patch the wind effect with a reference grid
            patchgrid = load_sagadata(INPUT + 'patch.sgrd')
            windef2 = patching(windef1, patchgrid)

            # Define output file path
            outputfile2 = f"{OUTPUT}we/CHELSA_we_{day}_{month}_{year}_V.2.1.tif"

            # Convert and export as COG (Cloud-Optimized GeoTIFF)
            convert2cog(
                obj=windef2,
                tmp=TEMP,
                filename=outputfile2,
                scale=0.001,
                offset=0.0,
                ot='UInt16',
                st=3,
                cf_standard_name='wind_effect',
                variable='we',
                variable_long_name='Wind Effect',
                variable_unit='1',
                compress='DEFLATE',
                day=day,
                month=month,
                year=year
            )

            # Track memory usage
            logtrack(process, 'windeffect: finished windeffect calculation', logfile)

            # Clean up memory
            saga_api.SG_Get_Data_Manager().Delete_All()

            # If overwrite is disabled and file exists, notify the user
        else:
            print(
                f"{bcolors.OKGREEN}overwrite = False and {outputfile2} exists = {os.path.isfile(outputfile2)}{bcolors.ENDC}")


    ### calculate surface wind speed ***********************************************************************************
    if calcsfcWind == 1:
        outputfile2 = OUTPUT + 'sfcWind/CHELSA_sfcWind_' + day + '_' + month + '_' + year + '_V.2.1.tif'
        if os.path.isfile(outputfile2) == False or overwrite == 1:
            # Display warning message
            print(
                f"{bcolors.WARNING}overwrite = True or {outputfile2} does not exist! Starting calculation{bcolors.ENDC}")

            # Track memory usage
            logtrack(process, 'wind speed: start calculation', logfile)

            # Define pressure levels for interpolation
            levels = list(range(17))  # Equivalent to range(0, 17, 1)
            for i in levels:
                print(i)

            # Import required datasets
            uz = import_ncdf(TEMP + 'uz.nc')  # Zonal wind component
            vz = import_ncdf(TEMP + 'vz.nc')  # Meridional wind component
            zg = import_ncdf(TEMP + 'geoz.nc')  # Geopotential height
            dem_geo = load_sagadata(INPUT + 'dem_geo.sgrd')  # Digital Elevation Model (DEM)

            # Debugging: Print types to check if data was loaded successfully
            print(f"Loaded variables: uz={type(uz)}, vz={type(vz)}, zg={type(zg)}, dem_geo={type(dem_geo)}")

            # Check if any of the imports failed (i.e., NoneType)
            if any(var is None for var in [uz, vz, zg, dem_geo]):
                raise ValueError("Error: One or more input datasets failed to load. Check file paths!")

            # Perform multi-level to surface interpolation for wind components
            u_sfc = Multi_Level_to_Surface_Interpolation(var=uz, zg=zg, dem_geo=dem_geo, levels=levels)
            v_sfc = Multi_Level_to_Surface_Interpolation(var=vz, zg=zg, dem_geo=dem_geo, levels=levels)

            # Debugging: Confirm interpolation results
            print(f"Interpolated variables: u_sfc={type(u_sfc)}, v_sfc={type(v_sfc)}")

            # Convert Cartesian wind components to polar coordinates (direction & magnitude)
            direct, leng = Gradient_Vector_from_Cartesian_to_Polar_Coordinates(u_sfc=u_sfc, v_sfc=v_sfc)

            # Debugging: Check wind speed calculation
            print(f"Converted wind variables: direction={type(direct)}, magnitude={type(leng)}")

            # Convert and export as COG (Cloud-Optimized GeoTIFF)
            convert2cog(
                obj=leng,
                tmp=TEMP,
                filename=outputfile2,
                scale=0.01,
                offset=0.0,
                ot='UInt16',
                st=3,
                cf_standard_name='wind_speed',
                variable='sfcWind',
                variable_long_name='Daily-Mean Near-Surface Wind Speed',
                variable_unit='m s-1',
                compress='DEFLATE',
                day=day,
                month=month,
                year=year
            )

            # Clean up memory
            saga_api.SG_Get_Data_Manager().Delete_All()

            # If overwrite is disabled and file exists, notify the user
        else:
            print(f"{bcolors.OKGREEN}overwrite = False and {outputfile2} exists{bcolors.ENDC}")


    ### calculate relative humidity ************************************************************************************
    if calchurs == 1:
        outputfile2 = OUTPUT + 'hurs/CHELSA_hurs_' + day + '_' + month + '_' + year + '_V.2.1.tif'
        if os.path.isfile(outputfile2) == False or overwrite == 1:
            # Display warnings
            print(
                f"{bcolors.WARNING}overwrite = True or {outputfile2} does not exist! Starting calculation{bcolors.ENDC}")
            print(f"{bcolors.WARNING}overwrite = True or {outputfile2} calculating relative humidity{bcolors.ENDC}")

            # Clean memory before processing
            saga_api.SG_Get_Data_Manager().Delete_All()

            # Track memory usage
            logtrack(process, 'hurs: start calculation', logfile)

            # Load input datasets
            rh = import_ncdf(TEMP + 'hursz.nc')  # Relative humidity
            zg = import_ncdf(TEMP + 'geoz.nc')  # Geopotential height
            dem_geo = load_sagadata(INPUT + 'dem_geo.sgrd')  # Digital elevation model (DEM)

            # Debugging: Print data types to ensure successful loading
            print(type(rh), type(zg), type(dem_geo))

            # Load wind effect and resample to high-resolution DEM
            windef = import_gdal(f"{OUTPUT}we/CHELSA_we_{day}_{month}_{year}_V.2.1.tif")
            windef1 = resample(windef, dem_geo)

            # Load and preprocess lower-resolution DEM
            dem_low = load_sagadata(INPUT + 'orography.sgrd')
            dem_low = change_data_storage(dem_low)
            dem_low = change_latlong(dem_low)

            # Debugging: Confirm all dataset types
            print(type(rh), type(zg), type(dem_geo), type(dem_low))

            # Define pressure levels for interpolation
            levels = list(range(17))  # Equivalent to range(0, 17, 1)
            for i in levels:
                print(i)

            # Perform multi-level to surface interpolation
            rh1 = Multi_Level_to_Surface_Interpolation(var=rh, zg=zg, dem_geo=dem_geo, levels=levels)

            # Apply grid calculations to clean and transform relative humidity
            rh1a = grid_calculator_simple(rh1, 'ifelse(a<0.1, 0.1, ifelse(a>99.9, 99.9, a))')
            rh2 = grid_calculator_simple(rh1a, 'log((a/100) / (1 - (a/100)))')

            # Normalize wind data and resample
            windnorm = Grid_Normalization(windef1)
            windnorm_coarse = resample_up(windnorm, dem_low, 4)

            # Compute final relative humidity adjustments
            rh3 = grid_calculator3(rh2, windnorm.asGrid(), windnorm_coarse, '(a * (b + (c - b) * (1 - c)) / c)')
            rh4 = grid_calculator_simple(rh3, '1 / (1 + exp(-a))')

            # Convert and export as COG (Cloud-Optimized GeoTIFF)
            convert2cog(
                obj=rh4,
                tmp=TEMP,
                filename=outputfile2,
                scale=0.01,
                offset=0.0,
                ot='UInt16',
                st=3,
                cf_standard_name='relative_humidity',
                variable='hurs',
                variable_long_name='Near-Surface Relative Humidity',
                variable_unit='%',
                compress='DEFLATE',
                day=day,
                month=month,
                year=year
            )

            # Final cleanup of memory
            saga_api.SG_Get_Data_Manager().Delete_All()

        # If overwrite is disabled and file exists, notify the user
        else:
            print(f"{bcolors.OKGREEN}overwrite = False and {outputfile2} exists{bcolors.ENDC}")

    ### calculate surface precipitation ********************************************************************************
    if calcprec == 1:
        outputfile2 = OUTPUT + 'prec/CHELSA_prec_' + day + '_' + month + '_' + year + '_V.2.1.tif'
        if os.path.isfile(outputfile2) == False or overwrite == 1:
            print(bcolors.WARNING + 'overwrite = True or '
                  + outputfile2 + ' does not exists! start calculation' + bcolors.ENDC)
            logtrack(process, 'precipitation: start calculation', logfile)
            # load data
            prec = import_ncdf(TEMP + 'ps.nc').Get_Grid(0)
            #prec = get_sum(prec)
            prec = change_latlong(prec)
            prec = grid_calculator_simple(prec, 'a*1000')

            dem_low = load_sagadata(INPUT + 'orography.sgrd')
            dem_low = change_data_storage(dem_low)
            dem_low = change_latlong(dem_low)
            dem_high = load_sagadata(INPUT + 'dem_latlong.sgrd')
            dem_high = change_data_storage(dem_high)
            windef = import_gdal(OUTPUT + 'we/CHELSA_we_' + day + '_' + month + '_' + year + '_V.2.1.tif')
            windef1 = resample(windef, dem_high)

            # import the boundary layer heigth
            pblh = import_ncdf(TEMP + 'blh.nc').Get_Grid(0)
            #pblh = get_mean(pblh)
            pblh = change_latlong(pblh)

            # correct wind effect by boundary layer height
            pblh = grid_calculator(pblh, dem_low, 'a+b')
            dist2bound = calc_dist2bound(dem_high, pblh)
            maxdist2bound = resample_up(dist2bound, pblh, 7)
            maxdist2bound2 = invert_dist2bound(dist2bound, maxdist2bound)

            # track memory usage
            logtrack(process, 'precipitation: windeffect correction start', logfile)

            wind_cor = grid_calculatorX(maxdist2bound2, windef1, '(b/(1-a/9000))')
            patchgrid = load_sagadata(INPUT + 'patch.sgrd')
            wind_cor = patching(wind_cor, patchgrid)
            exp_index = load_sagadata(INPUT + 'expocor.sgrd')
            wind_cor = grid_calculatorX(exp_index, wind_cor, 'a*b')
            wind_coarse = resample(wind_cor, dem_low)
            wind_coarse = closegaps(wind_coarse)

            # track memory usage
            logtrack(process, 'precipitation: windeffect correction end', logfile)

            # downscale precipitation and export
            precip = downscale_precip(wind_cor, wind_coarse, prec, 'total surface precipitation')

            convert2cog(obj=precip,
                        tmp=TEMP,
                        filename=outputfile2,
                        scale=0.01,
                        offset=0.0,
                        ot='UInt16',
                        st=3,
                        cf_standard_name='precipitation_flux',
                        variable='pr',
                        variable_long_name='Precipitation',
                        variable_unit='kg m-2 day-1',
                        compress='DEFLATE',
                        day=day,
                        month=month,
                        year=year)


            # track memory usage
            logtrack(process, 'precipitation: calculated precipitation', logfile)

            saga_api.SG_Get_Data_Manager().Delete_All()

        else:
            print(bcolors.OKGREEN + 'overwrite = False and ' + outputfile2 + ' exists' + bcolors.ENDC)


    ### calculate cloud cover ******************************************************************************************
    if calcrsds == 1:
        outputfile = OUTPUT + 'clt/CHELSA_clt_' + day + '_' + month + '_' + year + '_V.2.1.tif'
        outputfile4 = OUTPUT + 'rsds/CHELSA_rsds_' + day + '_' + month + '_' + year + '_V.2.1.tif'
        if os.path.isfile(outputfile) == False or overwrite == 1:
            print(bcolors.WARNING + 'overwrite = True or '
                  + outputfile + ' does not exists! start calculation' + bcolors.ENDC)
            logtrack(process, 'cloud cover: start calculation', logfile)

            ## load data
            dem = load_sagadata(INPUT + 'dem_latlong3.sgrd')
            dem_geo = calc_geopotential(dem)
            windef = import_gdal(OUTPUT + 'we/CHELSA_we_' + day + '_' + month + '_' + year + '_V.2.1.tif')
            cblev_ras = load_sagadata(TEMP + 'cblev_ras.sgrd')

            cblev_res = resample(cblev_ras, dem)
            windef = resample_up(windef, dem_geo, 0)
            ccmean = import_ncdf(TEMP + 'ccz.nc')
            geomean = import_ncdf(TEMP + 'geoz.nc')

            levels = range(0, 38, 1)
            for i in levels:
                print(i)

            print(saga_api.SG_Get_Data_Manager().Get_Summary().c_str())

            cctotal = cloud_overlap(ccmean, geomean, cblev_res, dem_geo, windef, levels)
            cctotal = change_data_storage(cctotal)

            # track memory usage
            logtrack(process, 'cloud cover: finished clt calculation', logfile)

            ## bias correction of cc
            ccera = import_ncdf(TEMP + 'tcc.nc').Get_Grid(0)
            #ccera = get_mean(ccera)
            ccera = change_data_storage(ccera)
            ccera = change_latlong(ccera)
            cctot_resamp = resample(cctotal, ccera)
            cctot_resamp = change_data_storage(cctot_resamp)
            ccbias = calculate_bias(ccera, cctot_resamp)

            ccbiascor = biascor(cctotal, ccbias)
            ccbiascor = capat1(ccbiascor)

            convert2cog(obj=ccbiascor,
                        tmp=TEMP,
                        filename=outputfile,
                        scale=0.01,
                        offset=0.0,
                        ot='UInt16',
                        st=3,
                        cf_standard_name='cloud_area_fraction',
                        variable='clt',
                        variable_long_name='Total Cloud Cover Percentage',
                        variable_unit='percent',
                        compress='DEFLATE',
                        day=day,
                        month=month,
                        year=year)

            # track memory usage
            logtrack(process, 'cloud cover: finished clt biascor', logfile)

            saga_api.SG_Get_Data_Manager().Delete_All()

        else:
            print(bcolors.OKGREEN + 'overwrite = False and ' + outputfile + ' exists' + bcolors.ENDC)

        # Solar radiation ******************************************************************************************
    # load data
        if os.path.isfile(outputfile4) == False or overwrite == 1:
            print(bcolors.WARNING + 'overwrite = True or '
                      + outputfile4 + ' does not exists! start calculation' + bcolors.ENDC)
            logtrack(process, 'solar radiation: start calculation', logfile)

            srad_tot = import_gdal(SRAD + 'CHELSA_stot_pj_' + str(dayofyear) + '_V.2.1.tif')
            patchgrid = load_sagadata(INPUT + 'patch.sgrd')
            ssrd = import_ncdf(TEMP + 'ssrd.nc').Get_Grid(0)

            # track memory usage
            logtrack(process, 'rsds: rsds data loaded', logfile)

            # get cloud cover
            ccbiascor = import_gdal(OUTPUT + 'clt/CHELSA_clt_' + day + '_' + month + '_' + year + '_V.2.1.tif')

            # make calculation
            srad_sur = surface_radiation(srad_tot, ccbiascor, 'total downwelling solar radiation at surface')

            ## bias correct surface solar radiation downwards
            #ssrd = get_sum(ssrd)
            ssrd = change_latlong(ssrd)
            srad_resamp = resample(srad_sur, ssrd)
            srad_bias = simple_bias(srad_resamp, ssrd)
            srad_cor = biascor2(srad_sur, srad_bias)
            srad_cor = patching(srad_cor, patchgrid)
            srad_cor = change_data_storage2(srad_cor, 3)

            convert2cog(obj=srad_cor,
                        tmp=TEMP,
                        filename=outputfile4,
                        scale=1.0,
                        offset=0.0,
                        ot='UInt16',
                        st=3,
                        cf_standard_name='surface_downwelling_shortwave_flux_in_air',
                        variable='rsds',
                        variable_long_name='Surface Downwelling Shortwave Flux in Air',
                        variable_unit='MJ m-2',
                        compress='DEFLATE',
                        day=day,
                        month=month,
                        year=year)

            # track memory usage
            logtrack(process, 'rsds: rsds calculated', logfile)

            saga_api.SG_Get_Data_Manager().Delete_All()

        else:
            print(bcolors.OKGREEN + 'overwrite = False and ' + outputfile4 + ' exists' + bcolors.ENDC)


    # Temperatures lapse rates *****************************************************************************************
    if calctasmax == 1 or calctas == 1 or calctasmin == 1 or calctz == 1 or calcps == 1:
        outputfile2 = OUTPUT + 'tz/CHELSA_tz_' + day + '_' + month + '_' + year + '_V.2.1.tif'
        if os.path.isfile(outputfile2) == False or overwrite == 1:
            print(bcolors.WARNING + 'overwrite = True or '
                  + outputfile2 + ' does not exists! start calculation' + bcolors.ENDC)
            logtrack(process, 'lapse rates: calculate temperature lapse rates', logfile)
            # calculate temperature lapse rate
            #dicto = {}
            #for timestamp in times:
            #filename = TEMP + 't_levels_' + timestamp + '.nc'
            tlevels = import_ncdf(TEMP + 't_levels.nc')
            #filename = TEMP + 'z_levels_' + timestamp + '.nc'
            zlevels = import_ncdf(TEMP + 'z_levels.nc')

            zlevels.Get_Grid(0).Set_Scaling(zlevels.Get_Grid(0).Get_Scaling() * 0.10197162129)
            zlevels.Get_Grid(1).Set_Scaling(zlevels.Get_Grid(1).Get_Scaling() * 0.10197162129)
            #    dicto["lapse{0}".format(timestamp)] =
            tlapse_mean = tlapse(zlevels.Get_Grid(0), zlevels.Get_Grid(1), tlevels.Get_Grid(0), tlevels.Get_Grid(1), '(d-c)/(b-a)')

            # get lapserate at mean
            #tlapse_mean = get_mean_dicto(dicto, times)
            tlapse_mean = change_latlong(tlapse_mean)
            tlapse_mean = change_data_storage(tlapse_mean)
            tlapse_mean.Save(TEMP + 'tlapse_test.sgrd')

            convert2cog(obj=tlapse_mean,
                        tmp=TEMP,
                        filename=outputfile2,
                        scale=1.0,
                        offset=0.0,
                        ot='Float32',
                        st=7,
                        cf_standard_name='air_temperature_lapse_rate',
                        variable='tz',
                        variable_long_name='Air Temperature Lapse Rate',
                        variable_unit='K m-1',
                        compress='DEFLATE',
                        day=day,
                        month=month,
                        year=year)

            # track memory usage
            logtrack(process, 'lapse rates: finished calculating temperature lapse rates', logfile)

            saga_api.SG_Get_Data_Manager().Delete_All()

        else:
            print(bcolors.OKGREEN + 'overwrite = False and ' + outputfile2 + ' exists' + bcolors.ENDC)


    # Air pressure *****************************************************************************************************
    if calcps == 1:
        outputfile2 = OUTPUT + 'ps/CHELSA_ps_' + day + '_' + month + '_' + year + '_V.2.1.tif'
        if os.path.isfile(outputfile2) == False or overwrite == 1:
            print(bcolors.WARNING + 'overwrite = True or '
                  + outputfile2 + ' does not exists! start calculation' + bcolors.ENDC)
            # track memory usage
            logtrack(process, 'ps: calculation started', logfile)

            # import data, get mean over time, change longitudinal range to -180 - 180
            ps0 = import_ncdf(TEMP + 'ps0.nc').Get_Grid(0)
            #ps0 = get_mean(ps0)
            ps0 = change_latlong(ps0)
            tlapse_mean = import_gdal(OUTPUT + 'tz/CHELSA_tz_' + day + '_' + month + '_' + year + '_V.2.1.tif')
            tz = grid_calculator_simple(tlapse_mean, 'a*(-1)')
            tmean = import_ncdf(TEMP + 'tmean.nc').Get_Grid(0)
            #tmean = get_mean(tmean)
            tmean = change_latlong(tmean)

            # import dems
            dem_high = load_sagadata(INPUT + 'dem_latlong.sgrd')
            dem_high = change_data_storage(dem_high)
            dem_low = load_sagadata(INPUT + 'orography.sgrd')
            dem_low = change_data_storage(dem_low)
            dem_low = change_latlong(dem_low)

            # calculate surface air pressure
            ps_high = Air_Pressure(ps=ps0,
                                   orog=dem_low,
                                   tas=tmean,
                                   tz=tz,
                                   dem_high=dem_high)

            #export_geotiff(ps_high, outputfile2)
            convert2cog(obj=ps_high,
                        tmp=TEMP,
                        filename=outputfile2,
                        scale=10.0,
                        offset=0.0,
                        ot='UInt16',
                        st=3,
                        cf_standard_name='surface_air_pressure',
                        variable='ps',
                        variable_long_name='Surface Air Pressure',
                        variable_unit='Pa',
                        compress='DEFLATE',
                        day=day,
                        month=month,
                        year=year)

            # track memory usage
            logtrack(process, 'ps: calculation finished', logfile)

            saga_api.SG_Get_Data_Manager().Delete_All()

        else:
            print(bcolors.OKGREEN + 'overwrite = False and ' + outputfile2 + ' exists' + bcolors.ENDC)


    # Maximum Temperatures *********************************************************************************************
    if calctasmax == 1:
        outputfile2 = OUTPUT + 'tasmax/CHELSA_tasmax_' + day + '_' + month + '_' + year + '_V.2.1.tif'
        if os.path.isfile(outputfile2) == False or overwrite == 1:
            print(bcolors.WARNING + 'overwrite = True or '
                  + outputfile2 + ' does not exists! start calculation' + bcolors.ENDC)
            # import dems
            dem_high = load_sagadata(INPUT + 'dem_latlong.sgrd')
            dem_high = change_data_storage(dem_high)
            dem_low = load_sagadata(INPUT + 'orography.sgrd')
            dem_low = change_data_storage(dem_low)
            dem_low = change_latlong(dem_low)

            tlapse_mean = import_gdal(OUTPUT + 'tz/CHELSA_tz_' + day + '_' + month + '_' + year + '_V.2.1.tif')

            # import temperature data
            tmax = import_ncdf(TEMP + 'tmax.nc').Get_Grid(0)
            tmax = change_latlong(tmax)

            # get maximum temp
            #textreme_max = get_minmax(tmax, 1)

            tmax_highres = lapse_rate_based_downscaling(dem=dem_high,
                                                        lapse=tlapse_mean,
                                                        reference_dem=dem_low,
                                                        temperature=tmax)

            convert2cog(obj=tmax_highres,
                        tmp=TEMP,
                        filename=outputfile2,
                        scale=0.1,
                        offset=273.15,
                        ot='UInt16',
                        st=3,
                        cf_standard_name='air_temperature',
                        variable='tasmax',
                        variable_long_name='Daily Maximum Near-Surface Air Temperature',
                        variable_unit='K',
                        compress='DEFLATE',
                        day=day,
                        month=month,
                        year=year)

            # track memory usage
            logtrack(process, 'tas: tasmax calculated', logfile)
            saga_api.SG_Get_Data_Manager().Delete_All()

        else:
            print(bcolors.OKGREEN + 'overwrite = False and ' + outputfile2 + ' exists' + bcolors.ENDC)


    # Mean Temperatures ************************************************************************************************
    if calctas == 1:
        outputfile2 = OUTPUT + 'tas/CHELSA_tas_' + day + '_' + month + '_' + year + '_V.2.1.tif'
        if os.path.isfile(outputfile2) == False or overwrite == 1:
            print(bcolors.WARNING + 'overwrite = True or '
                  + outputfile2 + ' does not exists! start calculation' + bcolors.ENDC)
            # import dems
            dem_high = load_sagadata(INPUT + 'dem_latlong.sgrd')
            dem_high = change_data_storage(dem_high)
            dem_low = load_sagadata(INPUT + 'orography.sgrd')
            dem_low = change_data_storage(dem_low)
            dem_low = change_latlong(dem_low)

            tlapse_mean = import_gdal(OUTPUT + 'tz/CHELSA_tz_' + day + '_' + month + '_' + year + '_V.2.1.tif')
            tmean1 = import_ncdf(TEMP + 'tmean.nc').Get_Grid(0)
            #tmean = get_mean(tmean1)
            tmean = change_latlong(tmean1)

            tmean_highres = lapse_rate_based_downscaling(dem=dem_high,
                                                         lapse=tlapse_mean,
                                                         reference_dem=dem_low,
                                                         temperature=tmean)

            convert2cog(obj=tmean_highres,
                        tmp=TEMP,
                        filename=outputfile2,
                        scale=0.1,
                        offset=273.15,
                        ot='UInt16',
                        st=3,
                        cf_standard_name='air_temperature',
                        variable='tas',
                        variable_long_name='Daily Mean Near-Surface Air Temperature',
                        variable_unit='K',
                        compress='DEFLATE',
                        day=day,
                        month=month,
                        year=year)

            # track memory usage
            logtrack(process, 'tas: tas calculated', logfile)
            saga_api.SG_Get_Data_Manager().Delete_All()

        else:
            print(bcolors.OKGREEN + 'overwrite = False and ' + outputfile2 + ' exists' + bcolors.ENDC)


    # Minimum Temperatures *********************************************************************************************
    if calctasmin == 1:
        outputfile2 = OUTPUT + 'tasmin/CHELSA_tasmin_' + day + '_' + month + '_' + year + '_V.2.1.tif'
        if os.path.isfile(outputfile2) == False or overwrite == 1:
            print(bcolors.WARNING + 'overwrite = True or '
                  + outputfile2 + ' does not exists! start calculation' + bcolors.ENDC)
            # import dems
            dem_high = load_sagadata(INPUT + 'dem_latlong.sgrd')
            dem_high = change_data_storage(dem_high)
            dem_low = load_sagadata(INPUT + 'orography.sgrd')
            dem_low = change_data_storage(dem_low)
            dem_low = change_latlong(dem_low)

            tlapse_mean = import_gdal(OUTPUT + 'tz/CHELSA_tz_' + day + '_' + month + '_' + year + '_V.2.1.tif')

            # import temperature data
            tmin = import_ncdf(TEMP + 'tmin.nc').Get_Grid(0)
            #textreme_min = get_minmax(tmin, 0)

            tmin_highres = lapse_rate_based_downscaling(dem=dem_high,
                                                         lapse=tlapse_mean,
                                                         reference_dem=dem_low,
                                                         temperature=tmin)

            convert2cog(obj=tmin_highres,
                        tmp=TEMP,
                        filename=outputfile2,
                        scale=0.1,
                        offset=273.15,
                        ot='UInt16',
                        st=3,
                        cf_standard_name='air_temperature',
                        variable='tasmin',
                        variable_long_name='Daily Minimum Near-Surface Air Temperature',
                        variable_unit='K',
                        compress='DEFLATE',
                        day=day,
                        month=month,
                        year=year)

            # track memory usage
            logtrack(process, 'tas: tasmin calculated', logfile)

            saga_api.SG_Get_Data_Manager().Delete_All()

    else:
        print(bcolors.OKGREEN + 'overwrite = False and ' + outputfile2 + ' exists')


    # remove the temporary directory
    if keeptemp != 1:
        print(bcolors.OKBLUE + 'keeptemp = False: Deleting temporary directory' + bcolors.ENDC)
        shutil.rmtree(TEMP)

    else:
        print(bcolors.WARNING + 'keeptemp = True: Keeping temporary directory! Do not use for production runs!'
              + bcolors.ENDC)


    print(bcolors.HEADER + "CALCULATION END TIME: " + str(datetime.datetime.now()) + bcolors.ENDC)

    return True