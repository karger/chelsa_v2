# chelsa_v2



## Overview
This package contains functions to create daily high-resolution climate data using teh CHELSA V2 algorithm.
It contains the main CHELSA code and a module to download the input data from ERA5 via the Copernicus CDS.
It is part of the
CHELSA Project: (CHELSA, <https://www.chelsa-climate.org/>).

## Prerequisites
to be able to use the automatic download option via the Copernicus CDS you will need an account there.
For more information follow the guidance here: https://cds.climate.copernicus.eu/api-how-to
You will need a minium of 64 GB of RAM for global calculations.

## Running CHELSA V2
Chelsa is can be run via python 3 or via the terminal. We recommend running it in the terminal in combination with
the related singularity container to resolve all dependencies correctly. For the automatic download a internet
connection is required.

Clone the code into a directory. for this example we use the home directory, but any will do.
```console
cd ~
git clone https://gitlabext.wsl.ch/karger/chelsa_v2.git
```

## Example in terminal mode in linux
Here we give an example of running chelsa for a single day, the 15.01.2023. chelsa.py takes several arguments.
You can find out about the parameters by typing the following in the chelsa_v2 root directory:

```console
python3 chelsa.py --help
```

In this example, we calculate all possible environmental variables and download the input data as well. We can set
the day, month, and year as variables:

```console
day=15
month=1
year=2023
```

CHELSA requires a set of input files as well as a temporary and an output directory. In a first step you need to 
create the necessary directories, e.g. in your home.

```console
cd ~ 
mkdir input
mkdir srad
mkdir output
mkdir temp
for var in tas tasmin tasmax prec rsds sfcWind hurs we tz 
do
mkdir ./output/$var
done
```

You can then get the input data by. You need to have wget installed. The input data for srad is currently 320 GB of 
data. Make sure you have that space available.

```console
cd ~/input
wget -i ~/chelsa_v2/aux/inputpaths.txt
cd ~/srad
wget -i ~/chelsa_v2/aux/sradpaths.txt
```


if you parallelize the script by running several days in parallel, the processes can use the same input and output
directory but NOT the same temporary directory. Therefore, it is best to create a temporary root first, and then
create a separate temporary directory within the temporary root for each instance of chelsa. E.g.:

```
mkdir ./temp/temp_${day}_${month}_${day}
```

The script can be then run by giving the respective function arguments to chelsa.py
```console
temp=$/temp/temp_${day}_${month}_${day}/
input=/home/$USER/input/
output=/home/$USER/output/
srad=/home/$USER/srad/

singularity exec -B /storage /home/karger/singularity/chelsa_V2.1_B.sif python3 /home/karger/scripts/chelsa_v2/chelsa.py -b $year -c $month -d $day -i $input -o $output -t $temp -s $srad -pr 1 -ps 1 -sf 1 -hs 1 -ta 1 -tx 1 -tm 1 -rs 1 -tz 1 -kt 1 -dl 1
```

If you use a singularity container you have do adjust as follows:
```console
singularity exec helsa_V2.1_B.sif python3 ~/chelsa_v2/chelsa.py -b $year -c $month -d $day -i $input -o $output -t $temp -s $srad -pr 1 -ps 1 -sf 1 -hs 1 -ta 1 -tx 1 -tm 1 -rs 1 -tz 1 -kt 1 -dl 1
```

## Example for use on a slurm cluster
Chelsa can easily be parallelized on a slurm cluster here is an example slurm script that works on the WSL cluster.
It takes 1 additional input , the 'startdate' from which the taskID (SLURM_ARRAY_TASK_ID) starts 
to count days , e.g.: 2018-12-31. 
An example slurm script is located in /chelsa_v2/slurm. You can see its content by typing:
```console
cat ./slurm/submit_chelsa.sh 
```

Which gives you the following output:
```bash
#!/bin/bash

#SBATCH --job-name=chV21
#SBATCH --chdir=~/
#SBATCH --partition=node
#SBATCH --qos=normal
#SBATCH --account=node
#SBATCH --mail-user=dirk.karger@wsl.ch
#SBATCH --ntasks=1
#SBATCH --cpus-per-task=20
#SBATCH --mem-per-cpu=3000
#SBATCH --time=696:24:15
#SBATCH --output=~/chV21_%A_%a.out

# set the input parameters
INPUT=$'/storage/karger/chelsa_V2/INPUT/'
OUTPUT=$'/storage/karger/chelsa_V2/OUTPUT_DAILY/'
TEMP=$'/home/$USER/scratch/'
SRAD=$'/storage/karger/chelsa_V2/OUTPUT_DAILY/srad/'
YEAR=$(date -d "${1} $SLURM_ARRAY_TASK_ID days" +%Y)
MONTH=$(date -d "${1} $SLURM_ARRAY_TASK_ID days" +%m)
DAY=$(date -d "${1} $SLURM_ARRAY_TASK_ID days" +%d)
TEMP=$TEMP/chelsa$DAY$MONTH$YEAR

# create the temporary directory
if [ ! -d "$TEMP" ]; then
  echo "$TEMP does not exist. creating..."
  mkdir -p $TEMP
fi

# run chelsa
singularity exec -B /storage /home/$USER/chelsa_v2/singularity/chelsa_V2.1.sif python3 /home/$USER/chelsa_v2/chelsa.py -b $year -c $month -d $day -i $input -o $output -t $temp -s $srad -pr 1 -ps 1 -sf 1 -hs 1 -ta 1 -tx 1 -tm 1 -rs 1 -tz 1 -kt 1 -dl 1
```
The slurm bash script contains parameters that are native to slurm, and parameters that are needed by chelsa, e.g. 
the INPUT and OUTPUT directory among others. The code works as a slurm array and the parameters YEAR, MONTH, and DAY 
are controlled via the SLURM_ARRAY_TASK_ID. To calculate for example the entire january of 2023 in parallel, you have
to set the startdate to the 2022-12-31 and call the slurm script can then be called in the following way:
```console
sbatch --array=1-31 ./slurm/submit_chelsa.sh '2022-12-31'
```