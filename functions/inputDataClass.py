#!/usr/bin/env python

# This file is part of chelsa_highres.

# chelsa_highres is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# chelsa_highres is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with chelsa_highres.  If not, see <https://www.gnu.org/licenses/>.

import xarray as xr
import sys
import datetime


class InputDatasetBaseConfig:
    """
    A base class for configuring input datasets.

    Attributes:
        flag (str): The flag to determine the configuration.
        var_names (dict): A dictionary of variable names for the configuration.
        plevs2extract (dict): A dictionary of pressure levels to extract.
        num_plevs (int): The number of pressure levels present in the input data set.
        input_path (str): The base path to the input folder structure.
    """

    def __init__(self, flag):
        """
        Initialize the InputDatasetBaseConfig object.

        Args:
            flag (str): The flag to determine the configuration.
        """

        self.flag = flag
        self.var_names = None  # This will hold the specific mapping for the flag
        self.plevs2extract = None
        self.num_plevs = None  # write method that extracts those levels
        self.input_path = None  # path to folder structure where input data is lying
        self.tmp_path = None  # path to temporay folder where the input data is ready for CHELSA to use it

    def set_input_path(self, input_path):
        """
        Set the path of the folder structure where the input files are stored.
        Args:
            input (str): The path to folder structure where input files are stored.
        Returns:
            None
        Notes:
            This method sets the path to the input folder where input files are stored.
        """

        self.input_path = input_path

    def set_tmp_path(self, tmp_path):
        """
        Set the path of the folder structure where the input files are stored.
        Args:
            tmp (str): The path to folder structure where tmp files are stored.
        Returns:
            None
        Notes:
            This method sets the path to the tmp folder where input files are stored and ready for CHELSA to use them.
        """

        self.tmp_path = tmp_path

    def set_var_names(self, var_names):
        """
        Set the variable names of the input files and maps them to the CHELSA convention.
        Args:
            var_names (dict(str)): dict containing the input variable names mapped to their CHELSA counterpart
        Returns:
            None
        Notes:
            This method sets the input variable names mapped to their CHELSA counterpart.
        """

        self.var_names = var_names

    def set_plevs2extract(self, plevs, unit):
        """
       Set the pressure levels to extract.
        Args:
            plevs (str): A string representing the key name for the pressure levels to extract.
            unit (str): A string representing the unit of the pressure levels.
        Returns:
            None
        Notes:
            This method sets the name of the pressure levels to be extracted which are closes to
            950 and 850 hPa and stores them in a dictionary.
        """
        if unit == 'hPa' or unit == 'mbar' or unit == 'millibars':

            self.plevs2extract = {plevs: [950, 850]}

        elif unit == 'Pa':

            self.plevs2extract = {plevs: [95000, 85000]}

        else:
            raise ValueError('Invalid unit. Please use hPa, mbar or Pa \
                             or define new unit in inputDataClass.InputDatasetBaseConfig.set_plevs2extract.')

    def set_num_plevs(self, num_plevs):
        """
        Sets the number of pressure levels used in the input data set.
        Args:
            num_plevs (int): int representing the number of pressure levels present in the input dataset
        Returns:
            None
        """

        self.num_plevs = num_plevs

    def set_meta_data(self, meta_data):
        """
        Set metadata for the input dataset.

        This method sets the metadata for the input dataset, including the forcing and frequency.
        Returns:
            None
        Notes:
            This method sets the 'meta' attribute of the class to a dictionary containing the metadata.
        """

        self.meta_data = meta_data


class EulerConfig(InputDatasetBaseConfig):
    """
    A class to configure input datasets for Euler data.

    Attributes:
        flag (str): The flag to determine the configuration.
        var_names (dict): A dictionary of variable names for the configuration.

    """

    def __init__(self, base_path, tmp_path, year, month, var, flag='euler'):

        """
        Initialize the EulerConfig object.

        Args:
            flag (str): The flag to determine the configuration. Default is 'euler'.
        """

        super().__init__(flag)

        self.set_var_names({
            'cc': 'cl',
            'hurs': 'hurs',
            'pr_': 'pr',
            'ps': 'ps',
            'rh': 'hur',
            't': 'ta',
            'tas_': 'tas',
            'tasmax': 'tasmax',
            'tasmin': 'tasmin',
            'tcc': 'clt',
            'uz': 'ua',
            'u': 'uas',
            'vz': 'va',
            'v': 'vas',
            'zg': 'zg',
            'z': 'zg',  # z are the two levels clostest to 950 and 850 hPa
            'plevs': 'level'
        })

        # print(f'printing variable names: {self.var_names}')
        # print(f'{var} mapping for ERA5: {self.var_names[var]}')

        self.create_input_path(base_path, year, month, var)
        # print(f'init input path: {self.input_path}')
        self.create_tmp_path(tmp_path, var)
        # print(f'init tmp path: {self.tmp_path}')

        self.set_plevs2extract('level', 'millibars')
        # self.set_num_plevs(37)

        self.set_meta_data({
            'forcing': 'ERA5',
            'freq': 'day'
        })

    def create_input_path(self, base_path, year, month=None, var=None):
        """
        Create input path from base path and folder structure.

        Args:
            base_path (str): path to root of input folder structure, provided by slurm.
            year (int): The year of the data.
            month (int, optional): The month of the data. Default is None.
            var (str, optional): The variable of the data. Default is None.

        Returns:
            str: The input path.

        Notes:
            This method creates the input path from the base path and the folder structure.
            The folder structure is defined as follows:
            {base_path}/{var}/day/native/{year}/{var}_day_era5_{year}{month:02d}.nc
        """

        file_path = f'{base_path}{self.var_names[var]}/day/native/{year}/'        # for the era5 storage from C2SM

        # files are copyed to node scratch
        # file_path = base_path
        file_name = f'{self.var_names[var]}_day_era5_{year}{month:02d}.nc'

        # print(f'Input Path: {file_path}{file_name}')

        self.set_input_path(f'{file_path}{file_name}')

        # return f'{file_path}{file_name}'

    def create_tmp_path(self, tmp_path, var=None):
        """
        Create temporary file name from tmp path and variable name.

        Args:
            tmp_path (str): path temporary folder, provided by slurm.
            var (str, optional): The variable name of the data. Default is None.

        Returns:
            str: The temporary file path.

        Notes:
            This method creates the temporary file path from temporary folder path provided by slurm.
            The path is defined as follows:
            {tmp_path}/{var}.nc
        """

        tmp_name = f'{var}.nc'

        # print(f'tmp path: {tmp_path}{tmp_name}')

        self.set_tmp_path(f'{tmp_path}{tmp_name}')

        # return f'{tmp_path}{tmp_name}'

    def adapt_cc_unit(self):
        """
        Adapts the cloud water content unit from % to fraction (0-1 or g/kg).

        If the dataset is available, it attempts to convert the varArray to fraction by dividing it by 100.
        If an exception occurs during the conversion, it prints an error message.
        If the dataset is not available, it prints a message indicating that no variable cl is available to select.

        :return: None
        :rtype: None
        """

        if self.dataset is not None:
            try:
                self.varArray = self.varArray / 100

            except Exception as e:
                print(f"Error adapting cl unit: {e}")
        else:
            print("No variable cl available to select.")

    def adapt_pr_unit(self):
        """
        Adapts the precipitation rate unit from kg/m²s to m/h.

        If the dataset is available, it attempts to convert the varArray by dividing it by
        density of water (1000 kg/m3) and multiplying by 3600 s/h to convert from s to h.
        If an exception occurs during the conversion, it prints an error message.
        If the dataset is not available, it prints a message indicating that no variable pr is available to select.

        :return: None
        :rtype: Noned
        """

        if self.dataset is not None:
            try:
                # self.varArray = self.varArray /1000 * 3600
                self.varArray = self.varArray * 3600  # UNITS ARE WEIRD...???


            except Exception as e:
                print(f"Error adapting pr unit: {e}")
        else:
            print("No variable pr available to select.")

    def adapt_geop_unit(self):
        """
        Adapt the geopotential unit by converting the variable array from
        geopotential height to geopotential energy.

        This function checks if the dataset is not None and then attempts to
        convert the geopotential variable array (varArray) by multiplying it
        by the standard acceleration due to gravity (9.80665 m/s²). This
        conversion is necessary for certain calculations that require
        geopotential energy instead of geopotential height.

        :return: None
        :rtype: None
        """

        if self.dataset is not None:
            try:
                self.varArray = self.varArray * 9.80665


            except Exception as e:
                print(f"Error adapting geopotential unit: {e}")
        else:
            print("No variable geopotential (zg or z) available to select.")


class LevanteConfig(InputDatasetBaseConfig):
    """
    Base class for Levante configuration.
    """

    def __init__(self, base_path, flag='levante'):
        """
        Initialize Levante configuration.
        """

        super().__init__(flag)

        self.var_names = {
            'temperature': 'tas',
            'humidity': 'hur',
            'cloud_cover': 'cl'
        }

        # self.set_base_path(base_path)
        # self.plevs2extract(None, None)
        # self.set_num_plevs(None)


class CESM2Config(InputDatasetBaseConfig):
    """
    Base class for CESM2 configuration.
    """

    def __init__(self, base_path, flag='CESM2'):
        """
        Initialize CESM2 configuration.
        """

        super().__init__(flag)

        self.var_names = {
            'temp': 'tas',
            'rel_hum': 'hur',
            'cloud': 'cl'
        }

        # self.set_base_path(base_path)
        # self.plevs2extract('lev', 'hPa')
        # self.set_num_plevs(32)

    def change2datetime(self):
        """
        Prepare user data to fit further read-in interface.

        This function is currently only for CESM2 data. It sets the date back to Jan 1st for the first entry of the year,
        as CESM2 stores values of a specific day with the date of the next day.

        Args:
            dataset (xarray.dataset): The dataset containing all input data.

        Returns:
            None

        Notes:
            This method modifies the 'time' coordinate of the dataset by assigning it the mean value of the 'time_bnds' variable.
            It then converts the 'time' index to a datetimeindex.
        """

        time = self.dataset.time_bnds.mean("bnds")
        # time = dataset.time_bnds.mean("nbnd")         # for original dataset without uas, vas and geopot modification
        self.dataset = self.dataset.assign_coords(time=time)

        dsTime = self.dataset.indexes['time'].to_datetimeindex()
        self.dataset['time'] = dsTime


class InputDataProcessor(EulerConfig, LevanteConfig, CESM2Config):
    """
    input data class to be downscaled with CHELSA
    """

    def __init__(self, flag, base_path, tmp_path, var, year, month, day, hour=None):
        """
        Constructor method

        :param filePath: path to input file that is going to be downscaled,
        :type filePath: string
        :param tmpPath: path to output file in tmp for CHELSA to be read in,
        :type tmpPath: string

        """
        super().__init__(base_path, tmp_path, year, month, var, flag)

        self.dataset = self.load_dataset()
        self.varArray = None

        if flag == 'CESM2':
            self.change2datetime(self)

        if var in ['cc', 'rh', 't', 'uz', 'vz', 'zg']:
            self.get_num_plevs()

        self.extract_var(var)

        if flag == 'euler':

            if var in ['t', 'z']:
                self.extract_levels()

                if var == 'z':
                    self.adapt_geop_unit()

            elif var == 'cc':

                self.adapt_cc_unit()

            elif var == 'pr_':
                self.adapt_pr_unit()

            elif var == 'zg':
                self.adapt_geop_unit()

        self.get_data_from_date(year, month, day, hour)

        self.write_dataset()

    def load_dataset(self):
        """
        open input data set
        :return: dataset
        :rtype: xarray dataset
        """

        # print(self.input_path)

        try:
            dataset = xr.open_dataset(self.input_path)
            return dataset
        except Exception as e:
            print(f"Error loading NetCDF data: {e}")
            return None

    def extract_var(self, var):
        """
        Extract a variable from the dataset.

        Args:
            var (str): The name of the variable to extract.

        Returns:
            None

        Notes:
            This method extracts the specified variable from the dataset and assigns it to the 'dataset' attribute.
        """
        print(f'extracting {self.var_names[var]}')
        self.varArray = self.dataset[self.var_names[var]]

    def get_data_from_date(self, year, month, day, hour=None):
        """
        open input data set and select a specific date
        :param year:
        :type year: int
        :param month:
        :type month: int
        :param day:
        :type day: intget_num_plevs
        :param hour:
        :type hour: int
        :return: dataset at specific date
        :rtype: xarray dataset
        """

        try:

            if hour is not None:
                self.dataset_date = self.varArray.sel(
                    time='{}-{:02d}-{:02d}T{:02d}:00:00'.format(year, month, day, hour))
                # print('hour {}'.format(hour))
            else:
                self.dataset_date = self.varArray.sel(time='{}-{:02d}-{:02d}'.format(year, month, day))
                self.dataset_date = self.dataset_date.mean(dim='time')

            # datasetDate = xr.open_dataset(self.filePath)
            return self.dataset_date

        except Exception as e:
            print(f"Error extracting date from NetCDF data: {e}")
            return None

    def extract_levels(self):
        """
        extract levels that are closest to 950 and 850 hPa from dataset
        :param datasetDate: dataset
        :type datasetDate: xarray dataset

        """
        # print(f'extracting p levels {self.plevs2extract}')

        if self.dataset is not None:
            try:
                self.varArray = self.varArray.sel(self.plevs2extract, method='nearest')

            except Exception as e:
                print(f"Error selecting pressure levels: {e}")
        else:
            print("No pressure levels available to select.")

    def write_dataset(self):
        """
        write dataset to temp folder for further processing
        :param datasetDate: dataset at specific date
        :type datasetDate: xarray dataset

        """
        if self.dataset_date is not None:
            try:
                self.dataset_date.to_netcdf(self.tmp_path)
                self.dataset_date.close()

            except Exception as e:
                print(f"Error writing NetCDF data: {e}")
        else:
            print("No dataset available to write.")

    def get_num_plevs(self):

        """
        Get the number of pressure levels in the input dataset.

        This method returns the number of pressure levels in the input dataset, if it exists and the 'plevs' variable is present in the dataset.

        Returns:
            int or None: The number of pressure levels in the input dataset, or None if the dataset or 'plevs' variable does not exist.

        Notes:
            This method checks if the 'dataset' attribute of the class is not None and if the 'plevs' variable is present in the dataset.
            If both conditions are met, the method sets the 'num_plevs' attribute of the class to the length of the 'plevs' variable and returns it.
            If either condition is not met, the method returns None.
        """

        if self.dataset is not None and self.var_names['plevs'] in self.dataset.dims:
            self.set_num_plevs(len(self.dataset[self.var_names['plevs']]))
            return self.num_plevs
        else:
            return None

    def get_meta_data(self):
        """
        Get metadata for the input dataset.

        This method returns the metadata for the input dataset, including the forcing and frequency.

        Returns:
            dict or None: A dictionary containing the metadata, or None if the metadata has not been set.

        Notes:
            This method returns the 'meta' attribute of the class as a dictionary containing the metadata.
            If the metadata has not been set, the method returns None.
        """

        if hasattr(self, 'meta_data'):
            # print('attribute meta_data found')
            return self.meta_data
        else:
            return None








