#!/usr/bin/env python

# This file is part of chelsa_v2.
#
# chelsa_v2 is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# chelsa_v2 is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with chelsa_v2.  If not, see <https://www.gnu.org/licenses/>.
import datetime
import cdsapi
import os

def get_ERA5_old(TEMP, day, month, year,
             calcprec, calcps, calcsfcWind, calchurs, calctas, calctasmax, calctasmin, calcrsds, calctz):
    """
    get ERA5 data, convert it, and write it to a temporary directory

    :param TEMP: path to the temporary directory, string
    :param day: day, integer
    :param month: month, integer
    :param year: year, integer
    :param calcprec: calculation of precipitation, integer, 0=false, 1=true
    :param calcps: calculation of surface pressure, integer, 0=false, 1=true
    :param calcsfcWind: calculation of surface wind speed, integer, 0=false, 1=true
    :param calchurs: calculation of relative humidity, integer, 0=false, 1=true
    :param calctas: calculation of tas, integer, 0=false, 1=true
    :param calctasmax: calculation of tasmax, integer, 0=false, 1=true
    :param calctasmin: calculation of tasmin, integer, 0=false, 1=true
    :param calcrsds: calculation of rsds, integer, 0=false, 1=true
    :param calctz: calculation of temperature lapse rates, integer, 0=false, 1=true

    :return: True
    :rtype: bool
    """
    print(datetime.datetime.now())
    c = cdsapi.Client()

    # set times of the day and levels
    times = ['00:00','01:00','02:00',
             '03:00','04:00','05:00',
             '06:00','07:00','08:00',
             '09:00','10:00','11:00',
             '12:00','13:00','14:00',
             '15:00','16:00','17:00',
             '18:00','19:00','20:00',
             '21:00','22:00','23:00']

    levels = ['1','2','3',
              '5','7','10',
              '20','30','50',
              '70','100','125',
              '150','175','200',
              '225','250','300',
              '350','400','450',
              '500','550','600',
              '650','700','750',
              '775','800','825',
              '850','875','900',
              '925','950','975',
              '1000']


    plevels_temp = ['850','950']

    # geopotential height for temperatures
    if calctasmax == 1 or calctas == 1 or calctasmin == 1 or calctz == 1 or calcps == 1:
        for timestamp in times:
            c.retrieve(
                'reanalysis-era5-pressure-levels',
                {
                    'product_type':'reanalysis',
                    'variable':
                        'geopotential'
                    ,
                    'pressure_level':plevels_temp,
                    'year':year,
                    'month':month,
                    'day':day,
                    'time':timestamp,
                    'format':'netcdf'
                },
                TEMP+'z_levels_'+timestamp+'.nc')

    # atmospheric temperature
    if calctasmax == 1 or calctas == 1 or calctasmin == 1 or calctz == 1 or calcps == 1:
        for timestamp in times:
            c.retrieve(
                'reanalysis-era5-pressure-levels',
                {
                    'product_type':'reanalysis',
                    'variable':
                        'temperature'
                    ,
                    'pressure_level':plevels_temp,
                    'year':year,
                    'month':month,
                    'day':day,
                    'time':timestamp,
                    'format':'netcdf'
                },
                TEMP+'t_levels_'+timestamp+'.nc')

    # cc fraction
    if calcrsds == 1:
        c.retrieve(
            'reanalysis-era5-pressure-levels',
            {
                'product_type':'reanalysis',
                'variable': 'fraction_of_cloud_cover',
                'pressure_level':levels,
                'year':year,
                'month':month,
                'day':day,
                'time':times,
                'format':'netcdf'
                },
            TEMP + 'cc_levels.nc')
        os.system("cdo -sellonlatbox,-180,180,-90,90 -timmean " + TEMP + "cc_levels.nc " + TEMP +
        "cc_timmean_pl.nc")

    # geopotential height
    if calcrsds == 1:
        c.retrieve(
            'reanalysis-era5-pressure-levels',
            {
                'product_type':'reanalysis',
                'variable': 'geopotential',
                'pressure_level':levels,
                'year':year,
                'month':month,
                'day':day,
                'time':times,
                'format':'netcdf'
            },
                TEMP + 'geo_levels.nc')
        os.system("cdo -sellonlatbox,-180,180,-90,90 -timmean " + TEMP + "geo_levels.nc " + TEMP +
        "geo_timmean_pl.nc")

    # tasmax
    if calctasmax == 1:
        c.retrieve(
            'reanalysis-era5-single-levels',
            {
                'product_type':'reanalysis',
                'variable':'maximum_2m_temperature_since_previous_post_processing',
                'year':year,
                'month':month,
                'day':day,
                'time':times,
                'format':'netcdf'
            },
            TEMP+'tmax.nc')

    # tasmin
    if calctasmin == 1:
        c.retrieve(
            'reanalysis-era5-single-levels',
            {
                'product_type':'reanalysis',
                'variable':'minimum_2m_temperature_since_previous_post_processing',
                'year':year,
                'month':month,
                'day':day,
                'time':times,
                'format':'netcdf'
            },
            TEMP+'tmin.nc')

    # tas
    if calctasmax == 1 or calctas == 1 or calctasmin == 1 or calctz == 1 or calcps == 1:
        c.retrieve(
            'reanalysis-era5-single-levels',
            {
                'product_type':'reanalysis',
                'variable':'2m_temperature',
                'year':year,
                'month':month,
                'day':day,
                'time':times,
                'format':'netcdf'
            },
            TEMP+'tmean.nc')

    # u wind
    if calchurs == 1 or calcprec == 1 or calcrsds == 1:
        c.retrieve(
            'reanalysis-era5-single-levels',
            {
                'product_type':'reanalysis',
                'variable':'10m_u_component_of_wind',
                'year':year,
                'month':month,
                'day':day,
                'time':times,
                'format':'netcdf'
            },
            TEMP+'u.nc')

    # v wind
    if calchurs == 1 or calcprec == 1 or calcrsds == 1:
        c.retrieve(
            'reanalysis-era5-single-levels',
            {
                'product_type':'reanalysis',
                'variable':'10m_v_component_of_wind',
                'year':year,
                'month':month,
                'day':day,
                'time':times,
                'format':'netcdf'
            },
            TEMP+'v.nc')

    # cloud base height
    if calchurs == 1 or calcprec == 1 or calcrsds == 1:
        c.retrieve(
            'reanalysis-era5-single-levels',
            {
                'product_type':'reanalysis',
                'variable':'cloud_base_height',
                'year':year,
                'month':month,
                'day':day,
                'time':times,
                'format':'netcdf'
            },
            TEMP+'cbase.nc')

    # total cloud cover
    if calcrsds == 1:
        c.retrieve(
            'reanalysis-era5-single-levels',
            {
                'product_type':'reanalysis',
                'variable':'total_cloud_cover',
                'year':year,
                'month':month,
                'day':day,
                'time':times,
                'format':'netcdf'
            },
            TEMP+'tcc.nc')

    # surface radiation
    if calcrsds == 1:
        c.retrieve(
            'reanalysis-era5-single-levels',
            {
                'product_type':'reanalysis',
                'variable':'surface_solar_radiation_downwards',
                'year':year,
                'month':month,
                'day':day,
                'time':times,
                'format':'netcdf'
            },
            TEMP+'ssrd.nc')

    # surface precipitation
    if calcprec == 1:
        c.retrieve(
            'reanalysis-era5-single-levels',
            {
                'product_type':'reanalysis',
                'variable':'total_precipitation',
                'year':year,
                'month':month,
                'day':day,
                'time':times,
                'format':'netcdf'
            },
            TEMP+'ps.nc')

    # boundary layer height
    if calchurs == 1 or calcprec == 1 or calcrsds == 1:
        c.retrieve(
            'reanalysis-era5-single-levels',
            {
                'product_type':'reanalysis',
                'variable':'boundary_layer_height',
                'year':year,
                'month':month,
                'day':day,
                'time':times,
                'format':'netcdf'
            },
            TEMP+'blh.nc')

    if calcps == 1:
        c.retrieve(
            'reanalysis-era5-single-levels',
            {
                'product_type': 'reanalysis',
                'format': 'netcdf',
                'variable': 'surface_pressure',
                'year': year,
                'month': month,
                'day': day,
                'time': times,
            },
            TEMP + 'ps0.nc')


    if calchurs == 1:
        c.retrieve(
            'reanalysis-era5-pressure-levels',
            {
                'product_type': 'reanalysis',
                'format': 'netcdf',
                'variable': 'relative_humidity',
                'pressure_level': levels,
                'year': year,
                'month': month,
                'day': day,
                'time': times,
            },
            TEMP+'hurs_pl.nc')
        os.system("cdo -sellonlatbox,-180,180,-90,90 -timmean " + TEMP + "hurs_pl.nc " + TEMP +
        "hurs_timmean_pl.nc")


    if calcsfcWind == 1:
        c.retrieve(
            'reanalysis-era5-pressure-levels',
            {
                'product_type': 'reanalysis',
                'format': 'netcdf',
                'variable': 'u_component_of_wind',
                'pressure_level': levels,
                'year': year,
                'month': month,
                'day': day,
                'time': times,
            },
            TEMP+'u_pl.nc')
        os.system("cdo -sellonlatbox,-180,180,-90,90 -timmean " + TEMP + "u_pl.nc " + TEMP +
        "u_timmean_pl.nc")

        c.retrieve(
            'reanalysis-era5-pressure-levels',
            {
                'product_type': 'reanalysis',
                'format': 'netcdf',
                'variable': 'v_component_of_wind',
                'pressure_level': levels,
                'year': year,
                'month': month,
                'day': day,
                'time': times,
            },
            TEMP + 'v_pl.nc')
        os.system("cdo -sellonlatbox,-180,180,-90,90 -timmean " + TEMP + "v_pl.nc " + TEMP + "v_timmean_pl.nc")

    return True






def get_ERA5_2(TEMP, day, month, year,
             calcprec, calcps, calcsfcWind, calchurs, calctas, calctasmax, calctasmin, calcrsds, calctz):
    """
    get ERA5 data, convert it, and write it to a temporary directory

    :param TEMP: path to the temporary directory, string
    :param day: day, integer
    :param month: month, integer
    :param year: year, integer
    :param calcprec: calculation of precipitation, integer, 0=false, 1=true
    :param calcps: calculation of surface pressure, integer, 0=false, 1=true
    :param calcsfcWind: calculation of surface wind speed, integer, 0=false, 1=true
    :param calchurs: calculation of relative humidity, integer, 0=false, 1=true
    :param calctas: calculation of tas, integer, 0=false, 1=true
    :param calctasmax: calculation of tasmax, integer, 0=false, 1=true
    :param calctasmin: calculation of tasmin, integer, 0=false, 1=true
    :param calcrsds: calculation of rsds, integer, 0=false, 1=true
    :param calctz: calculation of temperature lapse rates, integer, 0=false, 1=true

    :return: True
    :rtype: bool
    """
    print(datetime.datetime.now())
    c = cdsapi.Client()

    # set times of the day and levels
    times = ['00:00','01:00','02:00',
             '03:00','04:00','05:00',
             '06:00','07:00','08:00',
             '09:00','10:00','11:00',
             '12:00','13:00','14:00',
             '15:00','16:00','17:00',
             '18:00','19:00','20:00',
             '21:00','22:00','23:00']

    levels = ['1','2','3',
              '5','7','10',
              '20','30','50',
              '70','100','125',
              '150','175','200',
              '225','250','300',
              '350','400','450',
              '500','550','600',
              '650','700','750',
              '775','800','825',
              '850','875','900',
              '925','950','975',
              '1000']


    plevels_temp = ['850','950']

    # geopotential height for temperatures
    if calctasmax == 1 or calctas == 1 or calctasmin == 1 or calctz == 1 or calcps == 1:
        c.retrieve(
            'derived-era5-pressure-levels-daily-statistics',
            {
                    'product_type':'reanalysis',
                    'variable':
                        'geopotential'
                    ,
                    'pressure_level':plevels_temp,
                    'year':year,
                    'month':month,
                    'day':day,
                    "daily_statistic": "daily_mean",
                    "time_zone": "utc+00:00",
                    "frequency": "1_hourly",
                    'format':'netcdf'
                },
                TEMP+'z_levels.nc')

    # atmospheric temperature
    if calctasmax == 1 or calctas == 1 or calctasmin == 1 or calctz == 1 or calcps == 1:

        c.retrieve(
            'derived-era5-pressure-levels-daily-statistics',
            {
                'product_type':'reanalysis',
                'variable':
                 'temperature'
                    ,
                'pressure_level':plevels_temp,
                'year':year,
                'month':month,
                'day':day,
                "daily_statistic": "daily_mean",
                "time_zone": "utc+00:00",
                "frequency": "1_hourly",
                'format':'netcdf'
                },
            TEMP+'t_levels.nc')

    # cc fraction
    if calcrsds == 1:
        c.retrieve(
            'derived-era5-pressure-levels-daily-statistics',
            {
                'product_type':'reanalysis',
                'variable': 'fraction_of_cloud_cover',
                'pressure_level':levels,
                'year':year,
                'month':month,
                'day':day,
                "daily_statistic": "daily_mean",
                "time_zone": "utc+00:00",
                "frequency": "1_hourly",
                'format':'netcdf'
                },
            TEMP + 'cc_levels.nc')
        os.system("cdo -sellonlatbox,-180,180,-90,90 " + TEMP + "cc_levels.nc " + TEMP +
        "ccz.nc")

    # geopotential height
    if calcrsds == 1:
        c.retrieve(
            'derived-era5-pressure-levels-daily-statistics',
            {
                'product_type':'reanalysis',
                'variable': 'geopotential',
                'pressure_level':levels,
                'year':year,
                'month':month,
                'day':day,
                "daily_statistic": "daily_mean",
                "time_zone": "utc+00:00",
                "frequency": "1_hourly",
                'format':'netcdf'
            },
                TEMP + 'geo_levels.nc')
        os.system("cdo -sellonlatbox,-180,180,-90,90 " + TEMP + "geo_levels.nc " + TEMP +
        "geoz.nc")

    # tasmax
    if calctasmax == 1:
        c.retrieve(
            'derived-era5-single-levels-daily-statistics',
            {
                'product_type':'reanalysis',
                'variable':'maximum_2m_temperature_since_previous_post_processing',
                'year':year,
                'month':month,
                'day':day,
                "daily_statistic": "daily_mean",
                "time_zone": "utc+00:00",
                "frequency": "1_hourly",
                'format':'netcdf'
            },
            TEMP+'tmax.nc')

    # tasmin
    if calctasmin == 1:
        c.retrieve(
            'derived-era5-single-levels-daily-statistics',
            {
                'product_type':'reanalysis',
                'variable':'minimum_2m_temperature_since_previous_post_processing',
                'year':year,
                'month':month,
                'day':day,
                "daily_statistic": "daily_mean",
                "time_zone": "utc+00:00",
                "frequency": "1_hourly",
                'format':'netcdf'
            },
            TEMP+'tmin.nc')

    # tas
    if calctasmax == 1 or calctas == 1 or calctasmin == 1 or calctz == 1 or calcps == 1:
        c.retrieve(
            'derived-era5-single-levels-daily-statistics',
            {
                'product_type':'reanalysis',
                'variable':'2m_temperature',
                'year':year,
                'month':month,
                'day':day,
                "daily_statistic": "daily_mean",
                "time_zone": "utc+00:00",
                "frequency": "1_hourly",
                'format':'netcdf'
            },
            TEMP+'tmean.nc')

    # u wind
    if calchurs == 1 or calcprec == 1 or calcrsds == 1:
        c.retrieve(
            'derived-era5-single-levels-daily-statistics',
            {
                'product_type':'reanalysis',
                'variable':'10m_u_component_of_wind',
                'year':year,
                'month':month,
                'day':day,
                "daily_statistic": "daily_mean",
                "time_zone": "utc+00:00",
                "frequency": "1_hourly",
                'format':'netcdf'
            },
            TEMP+'u.nc')

    # v wind
    if calchurs == 1 or calcprec == 1 or calcrsds == 1:
        c.retrieve(
            'derived-era5-single-levels-daily-statistics',
            {
                'product_type':'reanalysis',
                'variable':'10m_v_component_of_wind',
                'year':year,
                'month':month,
                'day':day,
                "daily_statistic": "daily_mean",
                "time_zone": "utc+00:00",
                "frequency": "1_hourly",
                'format':'netcdf'
            },
            TEMP+'v.nc')

    # cloud base height
    if calchurs == 1 or calcprec == 1 or calcrsds == 1:
        c.retrieve(
            'derived-era5-single-levels-daily-statistics',
            {
                'product_type':'reanalysis',
                'variable':'cloud_base_height',
                'year':year,
                'month':month,
                'day':day,
                "daily_statistic": "daily_mean",
                "time_zone": "utc+00:00",
                "frequency": "1_hourly",
                'format':'netcdf'
            },
            TEMP+'cbase.nc')

    # total cloud cover
    if calcrsds == 1:
        c.retrieve(
            'derived-era5-single-levels-daily-statistics',
            {
                'product_type':'reanalysis',
                'variable':'total_cloud_cover',
                'year':year,
                'month':month,
                'day':day,
                "daily_statistic": "daily_mean",
                "time_zone": "utc+00:00",
                "frequency": "1_hourly",
                'format':'netcdf'
            },
            TEMP+'tcc.nc')

    # surface radiation
    if calcrsds == 1:
        c.retrieve(
            'derived-era5-single-levels-daily-statistics',
            {
                'product_type':'reanalysis',
                'variable':'surface_solar_radiation_downwards',
                'year':year,
                'month':month,
                'day':day,
                "daily_statistic": "daily_mean",
                "time_zone": "utc+00:00",
                "frequency": "1_hourly",
                'format':'netcdf'
            },
            TEMP+'ssrd.nc')

    # surface precipitation
    if calcprec == 1:
        c.retrieve(
            'derived-era5-single-levels-daily-statistics',
            {
                'product_type':'reanalysis',
                'variable':'total_precipitation',
                'year':year,
                'month':month,
                'day':day,
                "daily_statistic": "daily_mean",
                "time_zone": "utc+00:00",
                "frequency": "1_hourly",
                'format':'netcdf'
            },
            TEMP+'ps.nc')

    # boundary layer height
    if calchurs == 1 or calcprec == 1 or calcrsds == 1:
        c.retrieve(
            'derived-era5-single-levels-daily-statistics',
            {
                'product_type':'reanalysis',
                'variable':'boundary_layer_height',
                'year':year,
                'month':month,
                'day':day,
                "daily_statistic": "daily_mean",
                "time_zone": "utc+00:00",
                "frequency": "1_hourly",
                'format':'netcdf'
            },
            TEMP+'blh.nc')

    if calcps == 1:
        c.retrieve(
            'derived-era5-single-levels-daily-statistics',
            {
                'product_type': 'reanalysis',
                'format': 'netcdf',
                'variable': 'surface_pressure',
                'year': year,
                'month': month,
                'day': day,
                "daily_statistic": "daily_mean",
                "time_zone": "utc+00:00",
                "frequency": "1_hourly",
            },
            TEMP + 'ps0.nc')


    if calchurs == 1:
        c.retrieve(
            'derived-era5-pressure-levels-daily-statistics',
            {
                'product_type': 'reanalysis',
                'format': 'netcdf',
                'variable': 'relative_humidity',
                'pressure_level': levels,
                'year': year,
                'month': month,
                'day': day,
                "daily_statistic": "daily_mean",
                "time_zone": "utc+00:00",
                "frequency": "1_hourly",
            },
            TEMP+'hurs_pl.nc')
        os.system("cdo -sellonlatbox,-180,180,-90,90 " + TEMP + "hurs_pl.nc " + TEMP +
        "hursz.nc")


    if calcsfcWind == 1:
        c.retrieve(
            'derived-era5-pressure-levels-daily-statistics',
            {
                'product_type': 'reanalysis',
                'format': 'netcdf',
                'variable': 'u_component_of_wind',
                'pressure_level': levels,
                'year': year,
                'month': month,
                'day': day,
                "daily_statistic": "daily_mean",
                "time_zone": "utc+00:00",
                "frequency": "1_hourly",
            },
            TEMP+'u_pl.nc')
        os.system("cdo -sellonlatbox,-180,180,-90,90 " + TEMP + "u_pl.nc " + TEMP +
        "uz.nc")

        c.retrieve(
            'derived-era5-pressure-levels-daily-statistics',
            {
                'product_type': 'reanalysis',
                'format': 'netcdf',
                'variable': 'v_component_of_wind',
                'pressure_level': levels,
                'year': year,
                'month': month,
                'day': day,
                "daily_statistic": "daily_mean",
                "time_zone": "utc+00:00",
                "frequency": "1_hourly",
            },
            TEMP + 'v_pl.nc')
        os.system("cdo -sellonlatbox,-180,180,-90,90 " + TEMP + "v_pl.nc " + TEMP + "vz.nc")

    return True


import os
import datetime
import cdsapi

def get_ERA5(TEMP, day, month, year,
             calcprec, calcps, calcsfcWind, calchurs, calctas, calctasmax, calctasmin, calcrsds, calctz):
    """
    Get ERA5 data, convert it, and write it to a temporary directory only if the file does not already exist.
    """
    print(datetime.datetime.now())
    c = cdsapi.Client()

    times = [f"{hour:02d}:00" for hour in range(24)]
    levels = [str(level) for level in [1, 2, 3, 5, 7, 10, 20, 30, 50, 70, 100, 125, 150, 175, 200,
                                       225, 250, 300, 350, 400, 450, 500, 550, 600, 650, 700, 750,
                                       775, 800, 825, 850, 875, 900, 925, 950, 975, 1000]]
    plevels_temp = ['850', '950']

    retrievals = [
        (calctasmax or calctas or calctasmin or calctz or calcps, 'derived-era5-pressure-levels-daily-statistics',
         {'product_type': 'reanalysis', 'variable': 'geopotential', 'pressure_level': plevels_temp,
          'year': year, 'month': month, 'day': day, "daily_statistic": "daily_mean", "time_zone": "utc+00:00",
          "frequency": "1_hourly", 'format': 'netcdf'}, 'z_levels.nc'),

        (calctasmax or calctas or calctasmin or calctz or calcps, 'derived-era5-pressure-levels-daily-statistics',
         {'product_type': 'reanalysis', 'variable': 'temperature', 'pressure_level': plevels_temp,
          'year': year, 'month': month, 'day': day, "daily_statistic": "daily_mean", "time_zone": "utc+00:00",
          "frequency": "1_hourly", 'format': 'netcdf'}, 't_levels.nc'),

        (calcrsds, 'derived-era5-pressure-levels-daily-statistics',
         {'product_type': 'reanalysis', 'variable': 'fraction_of_cloud_cover', 'pressure_level': levels,
          'year': year, 'month': month, 'day': day, "daily_statistic": "daily_mean", "time_zone": "utc+00:00",
          "frequency": "1_hourly", 'format': 'netcdf'}, 'cc_levels.nc'),
    ]

    for condition, dataset, request, filename in retrievals:
        filepath = os.path.join(TEMP, filename)
        if condition and not os.path.exists(filepath):
            c.retrieve(dataset, request, filepath)

    # Additional retrievals with checks
    variables = {
        'tmax.nc': ('derived-era5-single-levels-daily-statistics', 'maximum_2m_temperature_since_previous_post_processing', calctasmax),
        'tmin.nc': ('derived-era5-single-levels-daily-statistics', 'minimum_2m_temperature_since_previous_post_processing', calctasmin),
        'tmean.nc': ('derived-era5-single-levels-daily-statistics', '2m_temperature', calctas or calctasmax or calctasmin or calctz or calcps),
        'u.nc': ('derived-era5-single-levels-daily-statistics', '10m_u_component_of_wind', calchurs or calcprec or calcrsds),
        'v.nc': ('derived-era5-single-levels-daily-statistics', '10m_v_component_of_wind', calchurs or calcprec or calcrsds),
        'cbase.nc': ('derived-era5-single-levels-daily-statistics', 'cloud_base_height', calchurs or calcprec or calcrsds),
        'tcc.nc': ('derived-era5-single-levels-daily-statistics', 'total_cloud_cover', calcrsds),
        'ssrd.nc': ('derived-era5-single-levels-daily-statistics', 'surface_solar_radiation_downwards', calcrsds),
        'ps.nc': ('derived-era5-single-levels-daily-statistics', 'total_precipitation', calcprec),
    }

    for filename, (dataset, variable, condition) in variables.items():
        filepath = os.path.join(TEMP, filename)
        if condition and not os.path.exists(filepath):
            c.retrieve(dataset, {
                'product_type': 'reanalysis', 'variable': variable, 'year': year, 'month': month, 'day': day,
                "daily_statistic": "daily_mean", "time_zone": "utc+00:00", "frequency": "1_hourly",
                'format': 'netcdf'
            }, filepath)

    return True