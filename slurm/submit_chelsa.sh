#!/bin/bash

#SBATCH --job-name=CH_V2
#SBATCH -A node # Node Account
#SBATCH --qos=normal # normal priority level
#SBATCH --mail-user=dirk.karger@wsl.ch
#SBATCH --mail-type=SUBMIT,END,FAIL
#SBATCH --output=/home/karger/logs/CHV2_%A_%a.out.out
#SBATCH --error=/home/karger/logs/CHV2_%A_%a.err
#SBATCH --time=639:00:00
#SBATCH --cpus-per-task=20
#SBATCH --mem-per-cpu=3G
#SBATCH --ntasks=1

# set the input parameters
STARTYEAR=$(($1 - 1))
DOWNLOAD=$2
OVERWRITE=$3
INPUT=$'/home/karger/INPUT/'
OUTPUT=$'/storage/karger/chelsa_V2/OUTPUT_DAILY/'
TEMP=$'/home/karger/scratch/'
SRAD=$'/storage/karger/chelsa_V2/OUTPUT_DAILY/srad/'
YEAR=$(date -d "${STARTYEAR}-12-31 $SLURM_ARRAY_TASK_ID days" +%Y)
MONTH=$(date -d "${STARTYEAR}-12-31 $SLURM_ARRAY_TASK_ID days" +%m)
DAY=$(date -d "${STARTYEAR}-12-31 $SLURM_ARRAY_TASK_ID days" +%d)
TEMP=$TEMP/chelsa$DAY$MONTH$YEAR/

# create the temporary directory
if [ ! -d "$TEMP" ]; then
  echo "$TEMP does not exist. creating..."
  mkdir -p $TEMP
fi

# check if download is needed
for VAR in tasmin tas tasmax ps tz rsds clt hurs sfcWind we prec 
do
file=${OUTPUT}${VAR}/CHELSA_${VAR}_${DAY}_${MONTH}_${YEAR}_V.2.1.tif
if [ ! -f "$file" ]
  then
  echo "$file does not exist. setting download to 1"
  #DOWNLOAD=1
  else
  echo "$file does exists."
fi
done


# run chelsa
singularity exec -B /storage /home/karger/singularity/chelsa_V2.1.sif python3 /home/karger/scripts/chelsa_v2/chelsa.py -b $YEAR -c $MONTH -d $DAY -i $INPUT -o $OUTPUT -t $TEMP -s $SRAD -pr 1 -ps 1 -sf 1 -hs 1 -ta 1 -tx 1 -tm 1 -rs 1 -tz 1 -kt 0 -dl ${DOWNLOAD} -ow ${OVERWRITE}
