import datetime
import os
def logtrack(process, logstring, logfile):
    if os.path.isfile(logfile) == False:
        open(logfile, 'x+')
        print("log file created successfully")
    tmem = datetime.datetime.now().strftime("%m/%d/%Y, %H:%M:%S") \
           + ' ' + "%0d" % (process.memory_info()[0] / 1000000000) \
           + ' ' + logstring + '\n'
    with open(logfile, 'a') as log:
        log.write(tmem)

    return True