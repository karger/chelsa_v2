from functions.saga_functions import *
from functions.convert2cog import convert2cog
import saga_api
import xarray as xr

def calcbias(pr, ref, tmp, aux, month, year, scale_pr, scale_ref, outputfile=False):
    """
    Calculates the precipiation bias between a reference and a pr grid

    :param pr: input file, string
    :param ref: input file with reference, string
    :param tmp: path to temporary directory, string
    :param aux: path to auxillary grid, string
    :param month: month, integer
    :param year: year, integer
    :param scale_pr: scaling parameter pr, float
    :param scale_ref: scaling parameter ref, float
    :param outputfile: path to outputfile, string, default=False

    :return: True if outfile, else CSG_Grid
    :rtype: bool, CSG_Grid
    """
    ds = xr.open_dataset(ref).sel(time=str(year) + "-" + str("%02d" % (int(month),)) + "-" + str("%02d" % (int(month),)))
    ds['precip'].to_netcdf(tmp + 'tmp_ref.nc')
    ds['numgauge'].to_netcdf(tmp + 'tmp_nga.nc')
    Load_Tool_Libraries(True)
    ds_ref = import_ncdf(tmp + 'tmp_ref.nc')
    ds_nga = import_ncdf(tmp + 'tmp_nga.nc')
    ds_pr = import_gdal(pr)
    ds_pr = ds_pr.Set_Scaling(scale_pr, 0.0)
    ds_refg = ds_ref.Get_Grid(0)
    ds_refg.Set_Scaling(scale_ref, 0.0)
    ds_ngag = ds_nga.Get_Grid(0)
    ds_ngag_na = grid_calculator_simple(ds_ngag, 'ifelse(a>1, a-a+1, a-a)')
    ds_ngag_na.Set_NoData_Value(0)
    ds_refg_na = grid_calculator_simple(ds_refg, 'ifelse(a<0, a-a-1, a*1)')
    ds_refg_na.Set_NoData_Value(-99999)
    refg = grid_calculator(ds_refg_na, ds_ngag_na, 'a*b')
    pr_c = resample(ds_pr, refg)
    bias_g = grid_calculator(refg, pr_c, '(a+1)/(b+1)')
    bias_g = grid_calculator_simple(bias_g, 'ifelse(a>5, a-a+5, a*1)')
    bias_shp = gridvalues_to_points(bias_g)
    set_coordinate_reference_shp(bias_shp, '+proj=longlat +datum=WGS84 +no_defs ')
    bias_shp_prj = coordinate_transformation_shapes(bias_shp, '+proj=aeqd +lat_0=90 +lon_0=0 +x_0=0 +y_0=0 +datum=WGS84 +units=m +no_defs ')
    dummygrid = load_sagadata(aux)
    bias_grd = multilevel_B_spline(bias_shp_prj, dummygrid)
    bias_grd_latlong = proj_2_latlong(bias_grd, ds_pr)

    convert2cog(obj=bias_grd_latlong,
                tmp=tmp,
                filename=outputfile,
                scale=0.0001,
                offset=0.0,
                ot='UInt16',
                st=3,
                cf_standard_name='-',
                variable='prBias',
                variable_long_name='precipitation bias',
                variable_unit='ratio',
                compress='DEFLATE',
                day=15,
                month=month,
                year=year)

    return True if outputfile != False else bias_grd_latlong

