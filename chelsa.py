#!/usr/bin/env python

# This file is part of chelsa_v2.
#
# chelsa_v2 is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# chelsa_v2 is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with chelsa_v2.  If not, see <https://www.gnu.org/licenses/>.

# *************************************************
# import libraries
# *************************************************

import saga_api
#import sys
#import os
import argparse 
import datetime
#import os.path
#import cdsapi
#import psutil
#import shutil
#from functions import chelsa_main
from functions.get_ERA5 import get_ERA5
#from functions.saga_functions import *
from functions.chelsa_main import chelsa
from functions.inputDataClass import *

# *************************************************
# Get the command line arguments
# *************************************************
ap = argparse.ArgumentParser(
    description='''# This python code for CHELSA_V2.1
the code is adapted to the ERA5 reanalysis and automatically downloads
data from the CDS via the CDS api. It aggregates hourly data to daily
resolution and then runs the CHELSA algorithm for min-, max-, and mean 
temperature, total downwelling solar radiation, total cloud cover, and 
oprional surface precipitation. The output directory needs the following 
subfolders: /srad_sf, /tcc, /tmin, /tmax, /tmean, /tz.
Dependencies for ubuntu_18.04:
libwxgtk3.0-dev libtiff5-dev libgdal-dev libproj-dev 
libexpat-dev wx-common libogdi3.2-dev unixodbc-dev
g++ libpcre3 libpcre3-dev wget swig-4.0.1 python2.7-dev 
software-properties-common gdal-bin python-gdal 
python2.7-gdal libnetcdf-dev libgdal-dev
python-pip cdsapi saga_gis-7.6.0
All dependencies are resolved in the chelsa_V2.1.cont singularity container
Tested with: singularity version 3.3.0-809.g78ec427cc
''',
    epilog='''author: Dirk N. Karger, dirk.karger@wsl.ch, Version 2.1'''
)


# collect the function arguments
ap.add_argument('-b', '--year', type=int, help="year, integer")
ap.add_argument('-c', '--month', type=int, help="month, integer")
ap.add_argument('-d', '--day', type=int,  help="day, integer")
ap.add_argument('-i', '--input', type=str, help="input directory, string")
ap.add_argument('-o', '--output', type=str,  help="output directory, string")
ap.add_argument('-t', '--temp', type=str, help="root for temporary directory, string")
ap.add_argument('-s', '--srad', type=str, help="srad input directory, string")
ap.add_argument('-pr', '--prec', type=int, help="shall precipitation be calculated, integer, 1 = yes, 0 = no")
ap.add_argument('-ps', '--ps0', type=int, help="shall surface pressure be calculated, integer, 1 = yes, 0 = no")
ap.add_argument('-sf', '--sfcWind', type=int, help="shall 10m surface wind speed be calculated, integer, 1 = yes, 0 = no")
ap.add_argument('-hs', '--hurs', type=int, help="shall relative humidity be calculated, integer, 1 = yes, 0 = no")
ap.add_argument('-ta', '--tas', type=int, help="shall tas be calculated, integer, 1 = yes, 0 = no")
ap.add_argument('-tx', '--tasmax', type=int, help="shall tasmax be calculated, integer, 1 = yes, 0 = no")
ap.add_argument('-tm', '--tasmin', type=int, help="shall tasmin be calculated, integer, 1 = yes, 0 = no")
ap.add_argument('-rs', '--rsds', type=int, help="shall rsds be calculated, integer, 1 = yes, 0 = no")
ap.add_argument('-tz', '--tz', type=int, help="shall lapse rates be calculated, integer, 1 = yes, 0 = no")
ap.add_argument('-kt', '--keeptmp', type=int, help="shall the temporary data be kept, integer, 1 = yes, 0 = no")
ap.add_argument('-dl', '--down', type=int, help="data be downloaded, integer, 1 = yes, 0 = no")
ap.add_argument('-ow', '--overwrite', type=int, help="overwrite exiting files, integer, 1 = yes, 0 = no")

args = ap.parse_args()
print(args)

# *************************************************
# Get times from arguments
# *************************************************
year = "%02d" % args.year
day = "%02d" % args.day
month = "%02d" % args.month
calcprec = args.prec
calcps = args.ps0
calcsfcWind = args.sfcWind
calchurs = args.hurs
calctas = args.tas
calctasmax = args.tasmax
calctasmin = args.tasmin
calcrsds = args.rsds
calctz = args.tz
download = args.down
keeptmp = args.keeptmp
overwrite = args.overwrite

# *************************************************
# Get day of the year
# *************************************************
dt = datetime.datetime(args.year, args.month, args.day)
tt = dt.timetuple()
dayofyear = tt.tm_yday
dayofyear = "%0d" % dayofyear

# *************************************************
# Set the directories from arguments
# *************************************************
INPUT = args.input
OUTPUT = args.output
TEMP = args.temp
SRAD = args.srad
#TEMP = TEMP + 'CHELSAV2' + year + month + day + '/' #make sure to provide a unique directory for each day!

# *************************************************
# Main script
# ************************************************
if __name__ == '__main__':
    saga_api.SG_Set_History_Depth(0)

    if download == 0:
        ## TEST input interface class
        meta = None
        num_levels = None

        variables = ['cc', 'hurs', 'pr_', 'ps', 'rh', 't', 'tas_', 'tasmax', 'tasmin', 'tcc', 'uz', 'u', 'vz', 'v',
                     'zg', 'z']
        for var in variables:
            # print(f'{var} {YEAR}-{MONTH}-{DAY}')
            # EulerConfig(user_data, TEMP, YEAR, MONTH,  var, 'euler')
            input_data = InputDataProcessor('euler', '/storage/era5/', TEMP, var, int(year), int(month), int(day))

            if var in ['cc', 'rh', 't', 'uz', 'vz', 'zg'] and num_levels is None:
                num_levels = input_data.get_num_plevs()
                print(f'setting levels {num_levels}')
                levels = range(0, num_levels)
            else:
                print(f'levels already set {num_levels}')

            if meta is None:
                meta = input_data.get_meta_data()
                # print(f'setting meta {meta}')


    if download == 1:
        get_ERA5(TEMP=TEMP,
                 day=day,
                 month=month,
                 year=year,
                 calcps=calcps,
                 calcsfcWind=calcsfcWind,
                 calchurs=calchurs,
                 calcprec=calcprec,
                 calctas=calctas,
                 calctasmax=calctasmax,
                 calctasmin=calctasmin,
                 calcrsds=calcrsds,
                 calctz=calctz)

    chelsa(TEMP=TEMP,
           OUTPUT=OUTPUT,
           INPUT=INPUT,
           SRAD=SRAD,
           dayofyear=dayofyear,
           calcprec=calcprec,
           calcps=calcps,
           calcsfcWind=calcsfcWind,
           calchurs=calchurs,
           calctas=calctas,
           calctasmax=calctasmax,
           calctasmin=calctasmin,
           calcrsds=calcrsds,
           calctz=calctz,
           day=day,
           month=month,
           year=year,
           keeptemp=keeptmp,
           overwrite=overwrite)
