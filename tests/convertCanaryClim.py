from functions.convert2cog_canaries import convert2cog
from saga_functions import import_gdal
from saga_functions import Load_Tool_Libraries

Load_Tool_Libraries(True)

import os

# Define input directory structure (change accordingly)
inputdir_base = "/mnt/lud12/orig/globe/chelsa/chelsa_V1.2/orig_ta_canaries_1979-2013/"

# Define variable mappings
variables = {
    "tmean": ("tas", "Daily Mean Near-Surface Air Temperature"),
    "tmax": ("tasmax", "Daily Maximum Near-Surface Air Temperature"),
    "tmin": ("tasmin", "Daily Minimum Near-Surface Air Temperature"),
    "prec": ("pr", "Daily Precipitation")
}

# Define islands
islands = ["teneriffe", "la_palma", "lanzarote", "la_gomera", "gran_canaria", "fuerteventura", "el_hierro"]

# Define months
months = list(range(1, 13))  # [1, 2, ..., 12]

# Temporary and output directories
tmp = "/home/karger/scratch/canary/"
outputdir_base = "/storage/karger/chelsa/canaryclim/cog/"

for var, (var2, var_long_name) in variables.items():
    inputdir = os.path.join(inputdir_base, var)  # Adjust folder per variable
    outputdir = os.path.join(outputdir_base, var2)

    # Ensure output directory exists
    os.makedirs(outputdir, exist_ok=True)

    for island in islands:
        for month in months:
            month2 = f"{month:02d}"  # Ensure two-digit month format

            # Construct input filename
            filename = f"CHELSA_{island}_{var}_{month}_1979_2013.tif"
            filein = os.path.join(inputdir, filename)

            # Construct output filename
            outname = f"CHELSACanaryClim_{island}_{var2}_{month2}_1979_2013_V.1.0.tif"
            outputfile = os.path.join(outputdir, outname)

            # Import raster file
            raster = import_gdal(filein)

            # Call convert2cog function
            convert2cog(obj=raster,
                        tmp=tmp,
                        filename=outputfile,
                        scale=0.1,
                        offset=273.15 if "ta" in var2 else 0,  # Temperature variables need Kelvin conversion
                        ot='UInt16',
                        st=3,
                        cf_standard_name='air_temperature' if "ta" in var2 else 'precipitation_flux',
                        variable=var2,
                        variable_long_name=var_long_name,
                        variable_unit='K' if "ta" in var2 else 'kg*m^⁻2*month^-1',
                        compress='DEFLATE')

            print(f"Processed: {outputfile}")



import os

def update_metadata_frequency(directory):
    """
    Update the 'frequency' metadata field from 'day' to 'normal' for all .tif files in a directory.

    :param directory: Path to the directory containing .tif files
    """
    for file in os.listdir(directory):
        if file.endswith(".tif"):  # Process only .tif files
            file_path = os.path.join(directory, file)
            command = f'gdal_edit.py -mo "frequency=normal" "{file_path}"'
            print(f"Updating metadata for: {file}")
            os.system(command)

# Example usage:
update_metadata_frequency("/storage/karger/chelsa/canaryclim/cog/tasmax")
update_metadata_frequency("/storage/karger/chelsa/canaryclim/cog/tasmin")
update_metadata_frequency("/storage/karger/chelsa/canaryclim/cog/tas")
update_metadata_frequency("/storage/karger/chelsa/canaryclim/cog/pr")