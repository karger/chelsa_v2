import xarray as xr
import numpy as np
import pandas as pd


def resample_and_apply_delta(high_res_path, low_res_path, output_path, start_date, method='additive'):
    """
    Resample high-resolution NetCDF to match the low-resolution NetCDF grid,
    calculate delta (difference), interpolate back to high resolution,
    and apply the delta to the original high-resolution dataset.

    Parameters:
        high_res_path (str): Path to the high-resolution NetCDF file.
        low_res_path (str): Path to the low-resolution NetCDF file.
        output_path (str): Path to save the modified high-resolution NetCDF file.
        start_date (str): Start date for the high-resolution dataset (YYYY-MM-DD).
        method (str): 'additive' for absolute difference, 'multiplicative' for ratio.
    """
    # Open the datasets
    high_res_ds = xr.open_dataset(high_res_path)
    low_res_ds = xr.open_dataset(low_res_path)

    # Extract variable names (assume first variable in each dataset)
    high_var = list(high_res_ds.data_vars.keys())[0]
    low_var = list(low_res_ds.data_vars.keys())[0]

    # Ensure high_res_ds time is in datetime64 format
    high_res_ds = high_res_ds.assign_coords(
        time=pd.date_range(start=start_date, periods=high_res_ds.sizes['time'], freq='D'))

    # Align time steps between high and low resolution datasets
    common_times = np.intersect1d(high_res_ds.time.values, low_res_ds.time.values)
    if len(common_times) == 0:
        raise ValueError("No common time steps found between high and low resolution datasets")

    high_res_ds = high_res_ds.sel(time=common_times)
    low_res_ds = low_res_ds.sel(time=common_times)

    # Identify actual spatial dimensions
    high_lat_dim = "lat" if "lat" in high_res_ds.dims else "y"
    high_lon_dim = "lon" if "lon" in high_res_ds.dims else "x"
    low_lat_dim = "lat" if "lat" in low_res_ds.dims else "y"
    low_lon_dim = "lon" if "lon" in low_res_ds.dims else "x"

    # Print identified dimensions
    print(f"High-res spatial dims: {high_lat_dim}, {high_lon_dim}")
    print(f"Low-res spatial dims: {low_lat_dim}, {low_lon_dim}")

    high_res_da = high_res_ds[high_var]
    low_res_da = low_res_ds[low_var]

    # Ensure coordinate variables exist before interpolation
    if low_lat_dim not in low_res_ds.coords or low_lon_dim not in low_res_ds.coords:
        raise ValueError(f"Missing coordinate variables in low-res dataset: {low_lat_dim}, {low_lon_dim}")

    # Resample high resolution to low resolution using interpolation
    high_res_coarse = high_res_da.interp({high_lat_dim: low_res_ds.coords[low_lat_dim],
                                          high_lon_dim: low_res_ds.coords[low_lon_dim]},
                                         method="linear")

    # Compute delta
    if method == 'additive':
        delta = low_res_da - high_res_coarse  # Absolute difference
    elif method == 'multiplicative':
        delta = low_res_da / high_res_coarse  # Ratio
        delta = delta.where(high_res_coarse != 0)  # Avoid division by zero
    else:
        raise ValueError("Method must be 'additive' or 'multiplicative'")

    # Interpolate delta back to high resolution using xarray's interp_like
    delta_high_res = delta.interp_like(high_res_da, method="linear")

    # Apply delta to the original high-resolution dataset
    if method == 'additive':
        adjusted_high_res = high_res_da + delta_high_res
    else:  # Multiplicative
        adjusted_high_res = high_res_da * delta_high_res

    # Save the output
    adjusted_high_res.to_netcdf(output_path)
    print(f"Processed file saved to: {output_path}")


# Example usage
resample_and_apply_delta(
    "/home/karger/scratch/tento90/CHELSA_HR_hurs_2027-01_V.1.0.nc",
    "/home/karger/scratch/tento90/hurs_basd.nc",
    "/home/karger/scratch/tento90/CHELSA_HR_hurs_ba_2027-01.nc",
    start_date="2027-01-01",
    method='additive'  # Change to 'multiplicative' if needed
)
