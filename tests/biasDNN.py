import pandas as pd
import xarray as xr
import numpy as np
import pickle
import tensorflow as tf
from tensorflow import keras
from tensorflow.keras import layers
import matplotlib.pyplot as plt
from functions.dnn_bias import *
from calendar import monthrange

# open the obs data
ds_obs = xr.open_dataset('/home/karger/scratch/gpcc/full_data_monthly_v2022_1941-2020_025.nc')

# open and modify the mod data
ds_mod = xr.open_dataset('/home/karger/scratch/gpcc/era5_tp_1941-2020_025.nc')
ds_covar1 = xr.open_dataset('/home/karger/scratch/gpcc/era5_si10_1941-2020_025.nc')
ds_covar2 = xr.open_dataset('/home/karger/scratch/gpcc/era5_t2m_1941-2020_025.nc')
ds_covar3 = xr.open_dataset('/home/karger/scratch/gpcc/era5_crr_1941-2020_025.nc')
ds_covar4 = xr.open_dataset('/home/karger/scratch/gpcc/era5_tcc_1941-2020_025.nc')

# modify the mod data
ds_mod = mod_ds(ds_mod)
ds_covar1 = mod_ds(ds_covar1)
ds_covar2 = mod_ds(ds_covar2)
ds_covar3 = mod_ds(ds_covar3)
ds_covar4 = mod_ds(ds_covar4)

# interpolate to match coordinates
ds_mod = ds_mod.interp(lat=ds_obs["lat"], lon=ds_obs["lon"])
ds_covar1 = ds_covar1.interp(lat=ds_obs["lat"], lon=ds_obs["lon"])
ds_covar2 = ds_covar2.interp(lat=ds_obs["lat"], lon=ds_obs["lon"])
ds_covar3 = ds_covar3.interp(lat=ds_obs["lat"], lon=ds_obs["lon"])
ds_covar4 = ds_covar4.interp(lat=ds_obs["lat"], lon=ds_obs["lon"])

# merge the data
ds = xr.merge([ds_obs, ds_mod, ds_covar1, ds_covar2, ds_covar3, ds_covar4])

# ds.to_netcdf('/home/karger/scratch/ds1.nc')
# select only where gauges are present
ds = ds.where(ds.numgauge > 0.0)

clipping = True

if clipping == True:
    min_lon = 5.0
    min_lat = 45.0
    max_lon = 15.0
    max_lat = 55.0

    mask_lon = (ds.lon >= min_lon) & (ds.lon <= max_lon)
    mask_lat = (ds.lat >= min_lat) & (ds.lat <= max_lat)

    ds = ds.where(mask_lon & mask_lat, drop=True)
    # select the fitting range
    ds.sel(time=slice('1940-01-01', '1990-01-31'))


ds.to_netcdf('/home/karger/scratch/ds_fitting.nc')


# model parameters
epochs=50
val_split=0.2


# run dnn on dataset
res_arr = xr.apply_ufunc(dnn_multifit,  # function to apply
                         ds.precip,
                         ds.tp,
                         ds.si10,
                         ds.t2m,
                         ds.crr,
                         ds.tcc,
                         ds.lat,
                         ds.lon,# pass arguments.
                         input_core_dims=[['time'], ['time'], ['time'], ['time'], ['time'], ['time'], [], []],
                         vectorize=True,
                         dask='parallelized',  # let dask handle the parallelization
                         dask_gufunc_kwargs=['allow_rechunk'])


# run dnn on dataset
res_arr = xr.apply_ufunc(dnn_fit,  # function to apply
                         ds.precip,
                         ds.tp,
                         ds.lat,
                         ds.lon,# pass arguments.
                         input_core_dims=[['time'], ['time'], [], []],
                         vectorize=True,
                         dask='parallelized',  # let dask handle the parallelization
                         dask_gufunc_kwargs=['allow_rechunk']) # data type of the output(s


# fitting finished *****************************************************************************************************

# STEP 2 Prediction

# open the obs data
ds_obs = xr.open_dataset('/home/karger/scratch/gpcc/full_data_monthly_v2022_1941-2020_025.nc')

# open and modify the mod data
ds_mod = xr.open_dataset('/home/karger/scratch/gpcc/era5_tp_1941-2020_025.nc')
ds_covar1 = xr.open_dataset('/home/karger/scratch/gpcc/era5_si10_1941-2020_025.nc')
ds_covar2 = xr.open_dataset('/home/karger/scratch/gpcc/era5_t2m_1941-2020_025.nc')
ds_covar3 = xr.open_dataset('/home/karger/scratch/gpcc/era5_crr_1941-2020_025.nc')
ds_covar4 = xr.open_dataset('/home/karger/scratch/gpcc/era5_tcc_1941-2020_025.nc')

# modify the mod data
ds_mod = mod_ds(ds_mod)
ds_covar1 = mod_ds(ds_covar1)
ds_covar2 = mod_ds(ds_covar2)
ds_covar3 = mod_ds(ds_covar3)
ds_covar4 = mod_ds(ds_covar4)

# interpolate to match coordinates
ds_mod = ds_mod.interp(lat=ds_obs["lat"], lon=ds_obs["lon"])
ds_covar1 = ds_covar1.interp(lat=ds_obs["lat"], lon=ds_obs["lon"])
ds_covar2 = ds_covar2.interp(lat=ds_obs["lat"], lon=ds_obs["lon"])
ds_covar3 = ds_covar3.interp(lat=ds_obs["lat"], lon=ds_obs["lon"])
ds_covar4 = ds_covar4.interp(lat=ds_obs["lat"], lon=ds_obs["lon"])

# merge the data
ds = xr.merge([ds_obs, ds_mod, ds_covar1, ds_covar2, ds_covar3, ds_covar4])

# ds.to_netcdf('/home/karger/scratch/ds1.nc')
# select only where gauges are present
ds = ds.where(ds.numgauge > 0.0)

clipping = True

if clipping == True:
    min_lon = 14.0
    min_lat = 54.0
    max_lon = 15.0
    max_lat = 55.0

    mask_lon = (ds.lon >= min_lon) & (ds.lon <= max_lon)
    mask_lat = (ds.lat >= min_lat) & (ds.lat <= max_lat)

    ds = ds.where(mask_lon & mask_lat, drop=True)
    # select the prediction range
    ds.sel(time=slice('1991-01-01', '2020-01-31'))


res_pred = xr.apply_ufunc(dnn_predict,
                          ds.tp,
                          ds.lat,
                          ds.lon,
                          input_core_dims=[['time'], [], []],
                          vectorize=True)

res_pred_ds = res_pred.to_dataset(name="bias")
res_pred_ds.to_netcdf("/home/karger/scratch/res_pred_ds.nc")


res_pred = xr.apply_ufunc(dnn_multipredict,
                          ds.tp,
                          ds.si10,
                          ds.t2m,
                          ds.crr,
                          ds.tcc,
                          ds.lat,
                          ds.lon,
                          input_core_dims=[['time'], ['time'], ['time'], ['time'], ['time'], [], []],
                          vectorize=True)




# Trashcan
# select a month for testing
mod_sgd = ds.sel(time='2011-01-01')

mod_sgd = mod_sgd.drop('time')

times = pd.date_range("2017/01/01","2017/01/01",freq='D')
time_da = xr.DataArray(times, [('time', times)])

mod_sgd = mod_sgd.expand_dims(time=time_da)
mod_sgd = mod_sgd.where(ds.numgauge > 0.0)
