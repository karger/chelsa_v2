


SLURM_ARRAY_TASK_ID=15
INPUT=$'/home/karger/scratch/chelsa_V2/input/'
OUTPUT=$'/home/karger/scratch/chelsa_V2/output/'
TEMP=$'/home/karger/scratch/chelsa_V2/temp/'
SRAD=$'/storage/karger/chelsa_V2/OUTPUT_DAILY/srad/'
YEAR=$(date -d "2022-12-31 $SLURM_ARRAY_TASK_ID days" +%Y)
MONTH=$(date -d "2022-12-31 $SLURM_ARRAY_TASK_ID days" +%m)
DAY=$(date -d "2022-12-31 $SLURM_ARRAY_TASK_ID days" +%d)

singularity exec -B /storage /home/karger/singularity/chelsa_V2.1.sif python3 /home/karger/scripts/chelsa_v2/chelsa.py -b $YEAR -c $MONTH -d $DAY -i $INPUT -o $OUTPUT -t $TEMP -s $SRAD -pr 1 -ps 1 -sf 1 -hs 1 -ta 1 -tx 1 -tm 1 -rs 1 -tz 1 -kt 1 -dl 0 -ow 0





SLURM_ARRAY_TASK_ID=16
INPUT=$'/storage/karger/chelsa_V2/INPUT/'
OUTPUT=$'/home/karger/scratch/output/'
TEMP=$'/home/karger/scratch/temp/'
SRAD=$'/storage/karger/chelsa_V2/OUTPUT_DAILY/srad/'
YEAR=$(date -d "2022-12-31 $SLURM_ARRAY_TASK_ID days" +%Y)
MONTH=$(date -d "2022-12-31 $SLURM_ARRAY_TASK_ID days" +%m)
DAY=$(date -d "2022-12-31 $SLURM_ARRAY_TASK_ID days" +%d)

singularity exec -B /storage /home/karger/singularity/chelsa_V2.1.sif python3 /home/karger/scripts/chelsa_v2/chelsa.py -b $YEAR -c $MONTH -d $DAY -i $INPUT -o $OUTPUT -t $TEMP -s $SRAD -pr 1 -ps 1 -sf 1 -hs 1 -ta 1 -tx 1 -tm 1 -rs 1 -tz 1 -kt 1 -dl 1 -ow 0

import saga_api
#import sys
#import os
import argparse
import datetime
#import os.path
#import cdsapi
#import psutil
#import shutil
#from functions import chelsa_main
from functions.get_ERA5 import get_ERA5
#from functions.saga_functions import *
from functions.chelsa_main import chelsa
import os.path
import psutil
import datetime
from functions.logtrack import logtrack


def Load_Tool_Libraries(Verbose):
    saga_api.SG_UI_Msg_Lock(True)        # Linux
    saga_api.SG_Get_Tool_Library_Manager().Add_Directory('/usr/local/lib/saga', False)
    # Or set the Tool directory like this!
    saga_api.SG_UI_Msg_Lock(False)
    if Verbose == True:
                print('Python - Version ' + sys.version)
                print(saga_api.SAGA_API_Get_Version())
                print('number of loaded libraries: ' + str(saga_api.SG_Get_Tool_Library_Manager().Get_Count()))
                print()
    return saga_api.SG_Get_Tool_Library_Manager().Get_Count()


def import_ncdf(ncdffile):
    Tool = saga_api.SG_Get_Tool_Library_Manager().Create_Tool('io_gdal', '6')
    if not Tool:
        print('Failed to request tool: Import NetCDF')
        return False
    Tool.Reset()
    Tool.Set_Parameter('FILE', ncdffile)
    Tool.Set_Parameter('SAVE_FILE', False)
    Tool.Set_Parameter('SAVE_PATH', '')
    Tool.Set_Parameter('TRANSFORM', True)
    Tool.Set_Parameter('RESAMPLING', 1)
    if not Tool.Execute():
        print('failed to execute tool: ' + Tool.Get_Name().c_str())
        return False
    Data = Tool.Get_Parameter('GRIDS').asGridList()
    return Data




TEMP='/home/karger/scratch/chelsa_V2/temp/'

Load_Tool_Libraries(True)
uwind = import_ncdf(TEMP + 'u.nc')


INPUT='/home/karger/scratch/chelsa_V2/input/'
OUTPUT='/home/karger/scratch/chelsa_V2/output/'
TEMP='/home/karger/scratch/chelsa_V2/temp/'
SRAD='/storage/karger/chelsa_V2/OUTPUT_DAILY/srad/'
YEAR=2023
MONTH=1
DAY=15



pr = '/storage/zilker/chelsaV2/outputMonthly/precSum/CHELSA_prec_01_2019_V.2.1.tif'
ref = '/home/karger/scratch/gpcc/full_data_monthly_v2022_2011_2020_025.nc'
tmp = '/home/karger/scratch/'
outputfile = '/home/karger/scratch/bias.tif'
aux = '/storage/karger/chelsa_V2/INPUT/azimuthal.sgrd'
month = 1
year = 2019
scale_pr = 0.01
scale_ref = 1.0

calcbias(pr, ref, tmp, aux, month, year, scale_pr, scale_ref, outputfile=False)

from functions.get_ERA5 import get_ERA5

get_ERA5(TEMP="/home/karger/scratch/tmptest/",
         day=1,
         month=1,
         year=2024,
         calcps=1,
         calcsfcWind=1,
         calchurs=1,
         calcprec=1,
         calctas=1,
         calctasmax=1,
         calctasmin=1,
         calcrsds=1,
         calctz=1)

chelsa(TEMP="/home/karger/scratch/tmptest/",
           OUTPUT="/home/karger/scratch/outtest/",
           INPUT='/storage/karger/chelsa_V2/INPUT/',
           SRAD='/storage/karger/chelsa_V2/OUTPUT_DAILY/srad/',
           dayofyear=1,
           calcprec=1,
           calcps=1,
           calcsfcWind=1,
           calchurs=1,
           calctas=1,
           calctasmax=1,
           calctasmin=1,
           calcrsds=1,
           calctz=1,
           day='01',
           month='01',
           year='2024',
           keeptemp=1,
           overwrite=0)


TEMP="/home/karger/scratch/tmptest/"
           OUTPUT="/home/karger/scratch/outtest/"
           INPUT='/storage/karger/chelsa_V2/INPUT/'
           SRAD='/storage/karger/chelsa_V2/OUTPUT_DAILY/srad/'
day = '01'
month = '01'
year = '2025'