#!/bin/bash
# CRON JOB for CHELSA

# set day to current date minus 7 days
DAY=$(date --date="7 days ago" +"%d")
MONTH=$(date --date="7 days ago" +"%m")
YEAR=$(date --date="7 days ago" +"%Y")

# set the input parameters
DOWNLOAD=1
OVERWRITE=0

# set the paths
INPUT='/storage/karger/chelsa_V2/INPUT/'
OUTPUT='/storage/karger/chelsa_V2/OUTPUT_DAILY/'
TEMPROOT='/home/karger/scratch/'
SRAD='/storage/karger/chelsa_V2/OUTPUT_DAILY/srad/'
TEMP=${TEMPROOT}chelsa$DAY$MONTH$YEAR/

# create the temporary directory
if [ ! -d "${TEMP}" ]; then
  echo "$TEMP does not exist. creating..."
  mkdir -p ${TEMP}
fi

# check if download is needed
for VAR in tasmin tas tasmax ps tz rsds clt hurs sfcWind we prec
do
file=${OUTPUT}${VAR}/CHELSA_${VAR}_${DAY}_${MONTH}_${YEAR}_V.2.1.tif
if [ ! -f "$file" ]
  then
  echo "$file does not exist. setting download to 1"
  DOWNLOAD=1
  else
  echo "$file does exists."
fi
done

# run chelsa
singularity exec -B /storage /home/karger/singularity/chelsa_V2.1.sif python3 /home/karger/scripts/chelsa_v2/chelsa.py -b ${YEAR} -c ${MONTH} -d ${DAY} -i ${INPUT} -o ${OUTPUT} -t "${TEMP}" -s ${SRAD} -pr 1 -ps 1 -sf 1 -hs 1 -ta 1 -tx 1 -tm 1 -rs 1 -tz 1 -kt 0 -dl ${DOWNLOAD} -ow ${OVERWRITE}

# copy to public bucket
for VAR in tasmin tas tasmax ps tz rsds clt hurs sfcWind prec
do
cp /storage/karger/chelsa_V2/OUTPUT_DAILY/${VAR}/CHELSA_${VAR}_${DAY}_${MONTH}_${YEAR}_V.2.1.tif /mnt/chelsa02/chelsa/global/daily/${VAR}/${YEAR}/
done

