from keras import layers
import keras
import tensorflow as tf
import numpy as np
import pandas as pd

def mod_ds(ds):
    ds.coords['longitude'] = (ds.coords['longitude'] + 180) % 360 - 180
    ds = ds.sortby(ds.longitude)
    ds = ds.rename({'longitude': 'lon', 'latitude': 'lat'})

    return ds


def dnn_fit(obs, mod, lat, lon, epochs=50, val_split=0.2, threshold=120, n_features=1):
    """
    fit a deep learning model and return a array of model weigths

    :param obs: observation data
    :param mod: model data
    :paran epochs: number of epochs, default=50
    :param val_split: validation split to be used, default=0.2
    :return: model weights
    :rtype: array
    """
    pred = np.array([np.nan])

    noobs = len(np.array(obs)[~np.isnan(np.array(obs))])
    print(noobs)

    if noobs >= threshold:
        # set up model
        model = keras.Sequential([
            layers.Dense(100, input_dim=n_features, activation='relu'),
            layers.Dense(80, activation='relu'),
            layers.Dense(1, activation="linear")
        ])
        model.compile(loss='mean_absolute_error',
                      optimizer=tf.keras.optimizers.Adam(0.01))

        bias = np.divide(np.add(obs, 1), np.add(mod, 1))
        dataset = pd.DataFrame({'mod':mod, 'bias':bias})
        dataset.dropna(axis = 0, how = 'any', inplace = True)
        train_dataset = dataset.sample(frac=0.8, random_state=0)
        train_features = train_dataset.copy()

        train_labels = train_features.pop('bias')
        labels = np.array(train_labels)
        features = np.array(train_features['mod'])

        model.fit(features,
            labels,
            validation_split=val_split,
            verbose=1, epochs=epochs)

        filepath = '/home/karger/keras_biasmodels/biasm_' + str(lat) + '_' + str(lon) + '_krs'
        model.save(filepath)
        pred = model.predict(mod)
        pred = np.array(pred)
        keras.backend.clear_session()

    return pred[0]


def dnn_predict(mod, lat, lon):
    """
    predicts a deep learning model and returns an array of model predictions

    :param obs: observation data
    :param mod: model data
    :param epochs: number of epochs, default=50
    :param val_split: validation split to be used, default=0.2
    :return: model weights
    :rtype: array
    """
    filepath = '/home/karger/keras_biasmodels/biasm_' + str(lat) + '_' + str(lon) + '_krs'
    model = keras.models.load_model(filepath)

    respred = model.predict(mod)
    keras.backend.clear_session()

    return respred


def dnn_multifit(obs, mod, covar1, covar2, covar3, covar4, lat, lon, epochs=50, val_split=0.2, threshold=120):
    """
    fit a deep learning model and return a array of model weigths

    :param obs: observation data
    :param mod: model data
    :paran epochs: number of epochs, default=50
    :param val_split: validation split to be used, default=0.2
    :return: model weights
    :rtype: array
    """
    noobs = len(np.array(obs)[~np.isnan(np.array(obs))])
    print(noobs)

    if noobs >= threshold:
        # set up model
        model = keras.Sequential([
            layers.LayerNormalization(axis=1),
            layers.Dense(100, input_shape=[5, ], activation='relu'),
            layers.Dense(80, activation='relu'),
            layers.Dense(1, activation="linear")
        ])
        model.compile(loss='mean_absolute_error',
                      optimizer=tf.keras.optimizers.Adam(0.001))

        bias = np.divide(np.add(obs, 1), np.add(mod, 1))

        dataset = pd.DataFrame({'mod':mod, 'bias':bias, 'covar1':covar1, 'covar2':covar2, 'covar3':covar3, 'covar4':covar4})
        dataset.dropna(axis = 0, how = 'any', inplace = True)
        train_dataset = dataset.sample(frac=0.8, random_state=0)
        train_features = train_dataset.copy()

        train_labels = train_features.pop('bias')
        labels = np.array(train_labels)

        model.fit(train_features,
            labels,
            validation_split=val_split,
            verbose=1, epochs=epochs)

        filepath = '/home/karger/keras_biasmodels_mult/biasm_' + str(lat) + '_' + str(lon) + '_krs'
        model.save(filepath)
        keras.backend.clear_session()

    return True


def dnn_multipredict(mod, covar1, covar2, covar3, covar4, lat, lon):
    """
    predicts a deep learning model and returns an array of model predictions

    :param obs: observation data
    :param mod: model data
    :param epochs: number of epochs, default=50
    :param val_split: validation split to be used, default=0.2
    :return: model weights
    :rtype: array
    """
    filepath = '/home/karger/keras_biasmodels_mult/biasm_' + str(lat) + '_' + str(lon) + '_krs'
    model = keras.models.load_model(filepath)
    features = pd.DataFrame(
        {'mod': mod, 'covar1': covar1, 'covar2': covar2, 'covar3': covar3, 'covar4': covar4})
    respred = model.predict(features)
    keras.backend.clear_session()

    return respred
